{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
 {* <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> *}
 <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"> 
<section class="container contact-form">
  <form action="{$urls.pages.contact}" method="post" {if $contact.allow_file_upload}enctype="multipart/form-data"{/if}>

    {if $notifications}
      <div class="col-xs-12 alert {if $notifications.nw_error}alert-danger{else}alert-success{/if}">
        <ul>
          {foreach $notifications.messages as $notif}
            <li>{$notif}</li>
          {/foreach}
        </ul>
      </div>
    {/if}

    {if !$notifications || $notifications.nw_error}
      <section class="container form-fields" id="bloc-contact">
      <h3>{l s='NOUS CONTACTER' d='Shop.Theme.Global'}</h3>
        <div class="row">
          <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
              
            </div>
          </div>

          <div class="col-md-4">
          <p>Par téléphone au <span id="number">0 800 108 200</span>* (* Appel gratuit) </p>
          <p>Du Lundi au vendredi de 9:00 à 12:30 et de 14:00 à 17:30 <br>
              Ou par email en remplissant le formulaire ci-dessous </p>
          </div>

          <div class="col-md-8">
            <div class="form-group">
              {* <label class="form-control-label">{l s='Subject' d='Shop.Forms.Labels'}</label> *}
              <div class="col-md-6">
                <select name="id_contact" class="form-control form-control-select">
                  {foreach from=$contact.contacts item=contact_elt}
                    <option value="{$contact_elt.id_contact}">{$contact_elt.name}</option>
                  {/foreach}
                </select>
                <input type="text" class="form-control user" name="nom" placeholder="Nom">
                <i class="fas fa-user"></i>
              </div>
            </div>

            {* {if $contact.orders} *}
            <div class="form-group">
              {* <label class="form-control-label">{l s='Order reference' d='Shop.Forms.Labels'}</label> *}
              <div class="col-md-6">
                {* <select name="id_order" class="form-control form-control-select">
                  <option value="">{l s='Select reference' d='Shop.Forms.Help'}</option>
                  {foreach from=$contact.orders item=order}
                    <option value="{$order.id_order}">{$order.reference}</option>
                  {/foreach}
                </select> *}
                <input type="text" class="form-control user" id="prenom" name="prenom" placeholder="Prénom">
                 <i class="fas fa-user" id="prenom"></i>
              </div>
              {* <span class="col-md-3 form-control-comment">
                {l s='optional' d='Shop.Forms.Help'}
              </span> *}
            </div>
            {* {/if} *}
          </div>
          

          {* <div class="col-md-4"></div> *}
          

          <div class="col-md-4"></div>
          <div class="col-md-8">
            <div class="form-group">
            {* <label class="form-control-label">{l s='Email address' d='Shop.Forms.Labels'}</label> *}
              <div class="col-md-6">
                <input
                  class="form-control user"
                  name="from"
                  type="email"
                  value="{$contact.email}"
                  placeholder="{l s='Email' d='Shop.Forms.Help'}"
                >
                <i class="fas fa-envelope"></i>
              </div>
            </div>

            {* {if $contact.phone} *}
            <div class="form-group">
              {* <label class="form-control-label">{l s='Attachment' d='Shop.Forms.Labels'}</label> *}
              <div class="col-md-6">
                {* <input type="file" name="fileUpload" class="filestyle" data-buttonText="{l s='Choose file' d='Shop.Theme.Actions'}"> *}
                <input type="number" name="phone" id="number" class="form-control user" placeholder="{l s='Téléphone' d='Shop.Forms.Help'}" value="{$contact.phone}">
                <i class="fas fa-phone" id="number"></i>
              </div>
              {* <span class="col-md-3 form-control-comment">
                {l s='optional' d='Shop.Forms.Help'}
              </span> *}
            </div>
            {* {/if} *}
          </div>
          
          <div class="col-md-4"></div>
          <div class="form-group">
            {* <label class="form-control-label">{l s='Message' d='Shop.Forms.Labels'}</label> *}
            <div class="col-md-8">
              <textarea
                class="form-control"
                id="bloc-message"
                name="message"
                placeholder="{l s='Votre Message' d='Shop.Forms.Help'}"
                rows="3"
              >{if $contact.message}{$contact.message}{/if}</textarea> <br>
              <div class="g-recaptcha g-recaptcha_contact" data-sitekey="6Ldx3f8UAAAAACQwade7YaYrhUH9L_YhCC5QqQdw"></div>
            </div>
          </div>
        </div>

        {if isset($id_module)}
          <div class="form-group row">
            <div class="offset-md-3">
              {hook h='displayGDPRConsent' id_module=$id_module}
            </div>
          </div>
        {/if}

      </section>

      <footer class="form-footer text-sm-right">
        <style>
          input[name=url] {
            display: none !important;
          }
        </style>
        <input type="text" name="url" value=""/>
        <input type="hidden" name="token" value="{$token}" />
        <input disabled class="btn btn-primary" id="envoyer" type="submit" name="submitMessage" value="{l s='Send' d='Shop.Theme.Actions'}">
      </footer>
    {/if}

  </form>
</section>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
        window.onload = function(){
            setInterval(function(){
                if(grecaptcha.getResponse()!=""){
                    $("#envoyer").removeAttr("disabled");
                }else{
                    $("#envoyer").attr("disabled","true");
                }
            },1000);
        }

    });
</script>