{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="_desktop_user_info" class="md_disptop_content">
  <div class="user-info">
    {if $logged}
        <a href="javascript:void(0)">
            <div class="dropdown">
                <i class="material-icons" id="customer_sing_in">&#xE7FF;</i>
                <div class="dropdown-content">
                    <ul>
                        <li id="identification_li">
                            <a
                                    class="account"
                                    href="{$my_account_url}"
                                    title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"
                                    rel="nofollow"
                            >
                                <span class="hidden-sm-down">{$customerName}</span>
                            </a>
                        </li>
                        <li id="identification_li">
                            <a
                                    class="logout hidden-sm-down"
                                    href="{$logout_url}"
                                    rel="nofollow"
                            >
                                {l s='Sign out' d='Shop.Theme.Actions'}
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </a>

    {else}
      <a
        href="javascript:void(0)"
        title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
          <div class="dropdown">
              <i class="material-icons" id="customer_sing_in">&#xE7FF;</i>
              <div class="dropdown-content">
                  <ul>
                      <li id="identification_li">
                          <a href="{$my_account_url}">
                              Identification
                          </a>
                      </li>
                      <li id="identification_li">
                          <a href="connexion?create_account=1">
                              Inscription
                          </a>
                      </li>
                  </ul>

              </div>
          </div>
          <script>

          </script>
      </a>
    {/if}
  </div>
</div>