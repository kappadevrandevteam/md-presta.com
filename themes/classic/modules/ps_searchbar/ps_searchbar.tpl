{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<!-- Block search module TOP -->
<div id="search_widget" class="search-widget md_disptop_content" data-search-controller-url="{$search_controller_url}">
	<div id="cherche_header">
		<i class="material-icons search" id="display_cherche_input">&#xE8B6;</i>
	</div>
	<form method="get" class="search-widget md_disptop_content" action="{$search_controller_url}" id="form_cherche" style="display: none">
		<input type="hidden" name="controller" value="search">
		<input type="text" id="input_cherche_affiche" name="s" value="{$search_string}" placeholder="{l s='Search our catalog' d='Shop.Theme.Catalog'}" aria-label="{l s='Search' d='Shop.Theme.Catalog'}">
		<button type="submit">
			{*<i class="material-icons search validate">&#xE8B6;</i>*}
      		<span class="hidden-xl-down">{l s='Search' d='Shop.Theme.Catalog'}</span>
		</button>
	</form>
</div>
<div id="_desktop_contact" class="md_disptop_content">
	<div class="user-info">
		<a href="/nous-contacter">
			{*<img src="/img/contactico.png" alt="contact" width="40" style="vertical-align: baseline;">*}
			<i class="material-icons mail">mail_outline</i>
		</a>
	</div>
</div>
{literal}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.0/jquery-migrate.js"></script>
	<script type="text/javascript">
		$('#display_cherche_input').click(function(){
		    if(document.getElementById('form_cherche').style.display === 'none'){
				$('#form_cherche').css('display','block');
            }else if(document.getElementById('form_cherche').style.display === 'block'){
                $('#form_cherche').css('display','none');
            }
        })
	</script>
{/literal}
<!-- /Block search module TOP -->
