
{*{if !$product.description && !$product.reference_to_display}*}
<div class="product-caracteristique">
    {if $category->id_parent === '4'}
	    {$content_caracteridtique nofilter}
	{elseif $category->id_parent === '8' || $category->id_category === '8'}
	    <div class="pt-0 tab-pane fade in{if !$product.description} active{/if}">
			<p>La <strong>{$product.name}</strong> est cousue par des couturières professionnelles qui apportent le plus grand soin à chaque produit.</p>
			<p>Que ce soit le tissu ou le PVC, la {$product.name} est conçue avec des procédés spécifiques. Il en résulte que les couleurs ne passent pas. Les guirlandes peuvent être utilisées tant en intérieur qu’en extérieur.</p>
			<p>Afin de vous faciliter la réception de votre {$product.name}, deux modes de<strong> livraison</strong> sont proposés. Mode de livraison via relais colis ou livraison à domicile</p>
			<h2 class="subtitle_caracteristique">Caractéristiques techniques de la {$product.name}</h2>
			<ul>
			<li class="nav-item">Taille de la guirlande : 6 m / 10 m</li>
			<li class="nav-item">Taille des fanions : 20×30 cm</li>
			<li class="nav-item">Nombre de fanions : 20 fanions</li>
			<li class="nav-item">Accroches : Fixation sur drisse blanche</li>
			<li class="nav-item">Matière : Tissu /Plastique</li>
			<li class="nav-item">Conditionnement : A l’unité</li>
			</ul>
		</div>
	{elseif $category->id_parent === '19' || $category->id_category === '19'}
		{if $category->id_category === '19' || $category->id_category === '30'}
		    <div class="pt-0 tab-pane fade in{if !$product.description} active{/if}">
				<p>Ce beach flag proposé par mon-drapeau.com est fabriqué en France</p>
				<p>La voile en impression numérique<strong> traversante</strong> permet une visibilité recto/verso en miroir</p>
				<p>Lavable à basse température et séchage à l'air libre</p>
				<h2>Caractéristiques <span class="foo">techniques</span></h2>
				<ul>
				<li class="nav-item">Voile de beach flag en tissu spécial drapeau 100% polyester.</li>
				<li class="nav-item">Finition <strong>ganse en élastique</strong> noire pour une résistance optimale.&nbsp;</li>
				<li class="nav-item">Mât en <strong>aluminium</strong>&nbsp; léger et solide.</li>
				<li class="nav-item">Poids total: 1.8kg (2.30M) / 2.3kg (3.00M)</li>
				<li class="nav-item">Montage/démontage facile et<strong> rapide</strong>.</li>
				</ul>
			</div>
		{elseif $category->id_category === '22'}
		    <div class="pt-0 tab-pane fade in{if !$product.description} active{/if}">
				<p>Le pied de beach flag est l'accessoire qui vient compléter le kit "mât + voile + pied". Trois modèles de pieds vons sont proposés en fonction de leur utilisation</p>
			</div>
		{/if}
	{/if}
</div>
{*{/if}*}