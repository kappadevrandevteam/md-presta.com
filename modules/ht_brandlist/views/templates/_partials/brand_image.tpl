
<style>
    .brand-section{
        height: 279px;
    }

    #brands-slider li.item{
        width:auto!important;
    }
    .bx-wrapper{
        max-width:100%!important;
        margin: auto!important;
    }
    .bx-wrapper .bx-pager{
        display: none !important;
    }
    .bx-wrapper .bx-controls-direction a:before{
        display:none !important;
    }

</style>
{literal}
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {
        $(document).ready(function(){
            $('#brand_wrapper').bxSlider({
                minSlides: 3,
                maxSlides:5,
                auto: 1,
                pause: 50,
                speed: 2000,
                autoHover: 1,
                randomStart : 1,
                /*slideWidth: 900,*/
                infiniteLoop: 1,
                slideMargin: 20
            });
        });
    });
</script>
{/literal}
<div class="marque-slide" style="clear:both; padding-top:25px;overflow: hidden">

    <ul id="brand_wrapper">
        {foreach from=$brands item=brand name=brand}
            <li class="item" style="margin-right: 20px;margin-left: 20px">
                <a href="{$brand['link']}" title="{$brand['name']}" class="">
                    <img src="{$brand['imageurl']}" alt="{$brand['name']}"  />

                </a>
            </li>
        {/foreach}
    </ul>

</div>
