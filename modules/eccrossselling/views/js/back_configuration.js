/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*/
(function($){
	$(document).ready(function(){
	   

        if($('#ECCROSSSELLING_ORDER_FOOTER_off').is(':checked'))
        {
            $('#ECCROSSSELLING_ORDER_FOOTER_off').parent().parent().parent().next().hide();
            $('#ECCROSSSELLING_ORDER_FOOTER_off').parent().parent().parent().next().next().hide();
        }
        
        
        if(!$('#type_spec').is(':checked'))
            $('#type_spec').parent().parent().parent().parent().next().hide();
        
        $('[id^=ECCROSSSELLING_ORDER_FOOTER]').click(function()
        {
             var id = $(this).attr('id');
             var tab = id.split('_');
             var oui = tab[3];
             
             if (oui == 'off'){
                $(this).parent().parent().parent().next().hide();
                $(this).parent().parent().parent().next().next().hide();
            }
             else{
                $(this).parent().parent().parent().next().show();
                if($('#type_spec').is(':checked'))
                    $(this).parent().parent().parent().next().next().show();
            }
        });
       
       $('#type_last').click(function()
        {
            $(this).parent().parent().parent().parent().next().hide();
        });
        $('#type_all').click(function()
        {
            $(this).parent().parent().parent().parent().next().hide();
        });
        $('#type_spec').click(function()
        {
            $(this).parent().parent().parent().parent().next().show();
        });
       
		var _$ = jQuery('#product-eccrossselling');
		_$.find('#product_eccrossselling_input').live('keyup', function(){
			var val = $.trim($(this).val());
			
            if (val.length) {
                $('.ajax-result').html('').hide();
				$.ajax({
					url: 'ajax_products_list.php',
					data: {q: encodeURIComponent(val), limit: 20, excludeIds: 9999999},
					success: function(data) {
						_$.find('.ajax-result').html('').hide();
						var products = data.split('\n');
						for (var i = 0; i < products.length - 1; i++) {
                            var _product = products[i].split('|');
                            var j = 0;
                            $('.ec_add_id_product').each(function( index, value ) {
                               if($(this).attr('value') == _product[1])
                                    j++;
                            });
                            if(j == 0)
    							$('.ajax-result').append('<a href="javascript:;" rel="'+_product[1]
    								+'"><img src="../img/admin/add.gif"> <span>'+_product[0]+'</span></a><br>').show();
						}
					}
				});
			} else {
				$('.ajax-result').html('').hide();
			}
		});
        
		
		_$.find('#product_eccrossselling_input').live('blur', function(){
			setTimeout(function(){
				$('.ajax-result').hide();
			}, 200);
		});
		
		$('.ajax-result a').live('click', function(){
			var id = $(this).attr('rel');
			var name = $(this).find('span').text();
            $('#ec_product_list').append('<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + id + '"><i class="icon-remove text-danger"></i></button>&nbsp;<input class="ec_add_id_product" type="hidden" name="ec_add_id_product[]" value="'+id+'" />'+id+' - '+ name +'</div>');
            $('#product_eccrossselling_input').val('');
		});
		
        $('.delAccessory').live('click', function(){
                $(this).parent().remove();
            });
		
	});
})(jQuery);