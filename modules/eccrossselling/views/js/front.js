/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*/

$(document).ready(function()
{ 
    $('#resultat #blockcart_scroll_right').click(function () {

        if($("#resultat ul").css('left') == '0px')
            $('#blockcart_scroll_left').show();
            
        var val = '-'+(($("#resultat ul li").length-4-1)*192)+'px';
        if (val == '-0px')
            val = '0px';
            
        if($("#resultat ul").css('left') == val)
            $('#blockcart_scroll_right').hide();   
        
        $('#resultat #blockcart_scroll_right').removeAttr('id');
        
        $("#resultat ul").animate({left:"-=192"},300, function() {
            $('#resultat .blockcart_scroll_right').attr('id', 'blockcart_scroll_right');
        });
        
        
    });

    $('#resultat #blockcart_scroll_left').click(function () {
        
        if($("#resultat ul").css('left') == '-192px')
            $('#blockcart_scroll_left').hide();

        if($("#resultat ul").css('left') == '-'+(($("#resultat ul li").length-4)*192)+'px')
            $('#blockcart_scroll_right').show();
        
        $('#resultat #blockcart_scroll_left').removeAttr('id');
        
        $("#resultat ul").animate({left:"+=192"},300, function() {
            $('#resultat .blockcart_scroll_left').attr('id', 'blockcart_scroll_left');
        });
        
    });
    
    
    $('#resultat_footer #blockcart_scroll_right_footer').click(function () {
        
        if($("#resultat_footer ul").css('left') == '0px')
            $('#blockcart_scroll_left_footer').show();
        
        var val = '-'+(($("#resultat_footer ul li").length-4-1)*192)+'px';
        if (val == '-0px')
            val = '0px';
        
        if($("#resultat_footer ul").css('left') == val)
            $('#blockcart_scroll_right_footer').hide();   
        
        $('#resultat_footer #blockcart_scroll_right_footer').removeAttr('id');
        
        $("#resultat_footer ul").animate({left:"-=192"},300, function() {
            $('#resultat_footer .blockcart_scroll_right_footer').attr('id', 'blockcart_scroll_right_footer');
        });
        
        
    });

    $('#resultat_footer #blockcart_scroll_left_footer').click(function () {
        
        if($("#resultat_footer ul").css('left') == '-192px')
            $('#blockcart_scroll_left_footer').hide();

        if($("#resultat_footer ul").css('left') == '-'+(($("#resultat_footer ul li").length-4)*192)+'px')
            $('#blockcart_scroll_right_footer').show();
        
        $('#resultat_footer #blockcart_scroll_left_footer').removeAttr('id');
        
        $("#resultat_footer ul").animate({left:"+=192"},300, function() {
            $('#resultat_footer .blockcart_scroll_left_footer').attr('id', 'blockcart_scroll_left_footer');
        });
        
    });

});