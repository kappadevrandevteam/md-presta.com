{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div id="product-eccrossselling" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="eccrossselling" />
    <input type="hidden" name="ec_is_add_new_product" id="ec_is_add_new_product" value="0" />
    <input type="hidden" name="ec_delete" value="0" />
	<h3>{l s='Ec Cross Selling' mod='eccrossselling'}</h3>
	<div class="form-group">
		<label class="control-label col-lg-3" for="product_autocomplete_input">
			<span class="label-tooltip" data-toggle="tooltip"
			title="{l s='Start by typing the first letters of the product\'s name, then select the product from the drop-down list.' mod='eccrossselling'}{l s='Do not forget to save the product afterwards!' mod='eccrossselling'}">
			{l s='Product search' mod='eccrossselling'}
			</span>
		</label>
		<div class="col-lg-5">
			<div id="ajax_choose_product">
				<div class="input-group">
                    <span class="input-group-addon"><i class="icon-search"></i></span>
					<input type="text" id="product_eccrossselling_input" name="product_eccrossselling_input" autocomplete="off">
                    <div class="ajax-result" style="display:none">
                    </div>
				</div>
                <div id="ec_product_list">
                {foreach from=$tab item=row}
                    <div class="form-control-static">
                        <button type="button" class="delAccessory btn btn-default" name="{$row.id_prod|escape:'htmlall':'UTF-8'}">
                        <i class="icon-remove text-danger"></i>
                        </button>
                        <input type="hidden" class="ec_add_id_product" name="ec_add_id_product[]" value="{$row.id_prod|escape:'htmlall':'UTF-8'}" />{$row.id_prod|escape:'htmlall':'UTF-8'} - {$row.name|escape:'html':'UTF-8'} ({$row.reference|escape:'html':'UTF-8'})
                    </div>
                    {/foreach}
                </div>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel' mod='eccrossselling'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" {$disabled|escape:'htmlall':'UTF-8'}><i class="{$icon|escape:'htmlall':'UTF-8'}"></i> {l s='Save' mod='eccrossselling'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" {$disabled|escape:'htmlall':'UTF-8'}><i class="{$icon|escape:'htmlall':'UTF-8'}"></i> {l s='Save and stay' mod='eccrossselling'}</button>
	</div>
</div>
