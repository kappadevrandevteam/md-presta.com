{*
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*}
<section class="page-product-box">
<div id="id_shop_ec" style="display:none">{$cart->id_shop|escape:'htmlall':'UTF-8'}</div>
<div id="id_lang_ec" style="display:none">{$cookie->id_lang|escape:'htmlall':'UTF-8'}</div>
<div id="static_token" style="display:none">{$static_token|escape:'htmlall':'UTF-8'}</div>
<div id="currency" style="display:none">{$currency->sign|escape:'htmlall':'UTF-8'}</div>
<div id="id_cart_ec" style="display:none">{$cart->id|escape:'htmlall':'UTF-8'}</div>

<script>
$(document).ready(function(){
  var id_shop = $('#id_shop_ec').html();
  var id_lang = $('#id_lang_ec').html();
  var id_cart = $('#id_cart_ec').html();
  var static_token = $('#static_token').html();
  var currency = $('#currency').html();
  var xhr = new XMLHttpRequest();
  xhr.open("GET",baseUri+'modules/eccrossselling/ajax_order_summary.php?bas='+baseUri+'&token='+static_token+'&id_shop='+id_shop+'&id_lang='+id_lang+'&id_cart='+id_cart+'&static_token='+static_token+'&sign='+currency, false);
  xhr.send(null);
  $('#resultat_footer').html(xhr.responseText);   
});
</script>

    <div id="resultat_footer" style="position: relative;"></div>
</section>