<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*/

if (!defined('_PS_VERSION_'))
    exit;

class Eccrossselling extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'eccrossselling';
        $this->tab = 'front_office_features';
        $this->version = '1.1.0';
        $this->author = 'Ether Creation';
        $this->module_key = '989c6a40a6ad89869fe96b0c9fe5aa01';
        $this->need_instance = 1;
        
        $this->l('Customers who bought this products also bought');
        $this->l('Previous');
        $this->l('Add to cart');
        $this->l('Next');
        $this->l('View');
        
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Ec Cross Selling');
        $this->description = $this->l('Chose products you want to cross sell for each product.');

        $this->confirmUninstall = $this->l('Are you sure you want to unistall module?');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('actionProductUpdate') ||
            !$this->registerHook('displayAdminProductsExtra') ||
            !$this->registerHook('header') ||
            !$this->registerHook('backOfficeHeader') ||
            !$this->registerHook('hook_eccrossselling') ||
            !$this->registerHook('displayFooterProduct') ||
            !$this->registerHook('displayShoppingCart'))
            return false;
        
            
        Configuration::updateValue('ECCROSSSELLING_CART', false);
        Configuration::updateValue('ECCROSSSELLING_FOOTER', false);

        $res = Db::getInstance()->execute('CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'eccrossselling` (
                                        `id_product` int(11) NOT NULL,
                                        `id_combination` int(11) NOT NULL,
                                        `id_shop` int(11) NOT NULL,
                                        PRIMARY KEY  (`id_product`, `id_combination`, `id_shop`)
                                    ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');
        
        $res &= Db::getInstance()->execute(''
            .'UPDATE `'._DB_PREFIX_.'hook` '
            .' SET `title` = "Hook Ec Cross Selling", `description` = "to have a hook in yout block cart", `position` = "1", `live_edit` = "0"'
            .' WHERE `name`= "hook_eccrossselling" ');
        
        
        @rename('../themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl', '../themes/'._THEME_NAME_.'/modules/blockcart/blockcartEC.tpl');
        Tools::copy('../modules/eccrossselling/views/templates/hook/blockcart.tpl', '../themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl');
        
        
        return (bool)$res;
    }

    public function uninstall()
    {
        @unlink('../themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl');
        @rename('../themes/'._THEME_NAME_.'/modules/blockcart/blockcartEC.tpl', '../themes/'._THEME_NAME_.'/modules/blockcart/blockcart.tpl');
        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitEccrosssellingModule')) == true)
            $this->_postProcess();

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');
                
        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitEccrosssellingModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'tab' => $this->display_combination(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display cross selling on layer cart'),
                        'name' => 'ECCROSSSELLING_CART',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display cross selling on product page footer'),
                        'name' => 'ECCROSSSELLING_FOOTER',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Display cross selling on order summary footer'),
                        'name' => 'ECCROSSSELLING_ORDER_FOOTER',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'radio',
                        'label' => $this->l('Displayed products on order summary footer'),
                        'name' => 'ECCROSSSELLING_ORDER_FOOTER_TYPE',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'type_last',
                                'value' => 0,
                                'label' => $this->l('Selected products of last product added to cart')
                            ),
                            array(
                                'id' => 'type_all',
                                'value' => 1,
                                'label' => $this->l('Selected products of all products added to cart')
                            ),
                            array(
                                'id' => 'type_spec',
                                'value' => 2,
                                'label' => $this->l('Specific selection of product, same for all cart')
                            )
                        ),
                    ),
                    array(
                        'type' => 'product_choice',
                        'label' => $this->l('Product search for order cart summary selection'),
                        'desc' => $this->l('Start by typing the first letters of the product\'s name, then select the product from the drop-down list.'),
                        'name' => 'product',
                        'id' => 'product',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'ECCROSSSELLING_CART' => Configuration::get('ECCROSSSELLING_CART'),
            'ECCROSSSELLING_FOOTER' => Configuration::get('ECCROSSSELLING_FOOTER'),
            'ECCROSSSELLING_ORDER_FOOTER' => Configuration::get('ECCROSSSELLING_ORDER_FOOTER'),
            'ECCROSSSELLING_ORDER_FOOTER_TYPE' => Configuration::get('ECCROSSSELLING_ORDER_FOOTER_TYPE'),
        );
    }

    /**
     * Save form data.
     */
    protected function _postProcess()
    {
        $form_values = $this->getConfigFormValues();
        $this->process_combination();
        foreach (array_keys($form_values) as $key)
            Configuration::updateValue($key, Tools::getValue($key));
    }
    
    public function process_combination()
    {
        if (Tools::isSubmit('ec_add_id_product'))
        {
            foreach (Tools::getValue('ec_add_id_product') as $id)
            {
                if (Db::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_combination = '.(int)$id.' AND id_product = '.(int)Tools::getValue('id_product').' AND id_shop = '.(int)$this->context->shop->id) == 0)
                    Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'eccrossselling` VALUES ('.(int)Tools::getValue('id_product').','.(int)$id.','.(int)$this->context->shop->id.')');
            }
            $products = Db::getInstance()->executeS('SELECT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product = '.(int)Tools::getValue('id_product').' AND id_shop = '.(int)$this->context->shop->id);
            $prod = array();
            foreach ($products as $product)
                $prod[] = $product['id_combination'];
            $tab = array_diff($prod, Tools::getValue('ec_add_id_product'));
            foreach ($tab as $id)
                Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_combination = '.(int)$id.' AND id_product = '.(int)Tools::getValue('id_product').' AND id_shop = '.(int)$this->context->shop->id);
        }
    }
    
    public function display_combination()
    {
        $products = Db::getInstance()->executeS('SELECT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product = '.(int)Tools::getValue('id_product').' AND id_shop = '.(int)$this->context->shop->id);
        $tab = array();
        foreach ($products as $product)
        {
            $query = 'SELECT name FROM `'._DB_PREFIX_.'product_lang` WHERE id_lang = '.(int)$this->context->language->id.' AND id_product = '.(int)$product['id_combination'].' AND id_shop = '.(int)$this->context->shop->id;
            $name = Db::getInstance()->getValue($query);
            $query2 = 'SELECT reference FROM `'._DB_PREFIX_.'product` WHERE id_product = '.(int)$product['id_combination'];
            $ref = Db::getInstance()->getValue($query2);
            $tab[] = array(
                'id_prod' => $product['id_combination'],
                'name' => $name,
                'reference' => $ref,
            );
        }
        $this->smarty->assign('tab', $tab);
        if (version_compare(_PS_VERSION_, '1.6.9', '>'))
        {
            $disabled = 'disabled="disabled"';
            $icon = 'process-icon-loading';
        }
        else
        {
            $disabled = '';
            $icon = 'process-icon-save';
        }
        $this->smarty->assign(array('link' => $this->context->link,
                                    'disabled' => $disabled,
                                    'icon' => $icon
                                    ));
        if ((int)Tools::getValue('id_product') == 0)
            return $tab;
        else
            return $this->display(__FILE__, 'views/templates/admin/tab_cross_selling.tpl');
    }
        
    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::isSubmit('updateproduct'))
        {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
        if (Tools::getValue('module_name') == $this->name)
        {
            $this->context->controller->addJS($this->_path.'views/js/back_configuration.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'views/js/front.js');
        $this->context->controller->addCSS($this->_path.'views/css/front.css');
    }
    
    

    public function hookActionProductUpdate()
    {
        $this->process_combination();
    }

    public function hookDisplayAdminProductsExtra()
    {
        return $this->display_combination();
    }
    
    public function hookhook_eccrossselling($params)
    {
        if (Configuration::get('ECCROSSSELLING_CART') == 1)
            return $this->display(__FILE__, 'views/templates/hook/ec_cross_selling.tpl');
    }
    
    public function hookDisplayFooterProduct()
    {
        $id_shop = $this->context->shop->id;
        $id_lang = $this->context->language->id;
        $iso_currency = $this->context->currency->iso_code;
        $this->smarty->assign(array('link' => $this->context->link,
            'id_shop' => $id_shop,
            'id_lang' => $id_lang,
            'iso_currency' => $iso_currency,
        ));
        if (Configuration::get('ECCROSSSELLING_FOOTER') == 1)
            return $this->display(__FILE__, 'views/templates/hook/ec_cross_selling_product_footer.tpl');
            
    }
    
    public function hookDisplayShoppingCart()
    {
        if (Configuration::get('ECCROSSSELLING_ORDER_FOOTER') == 1)
            return $this->display(__FILE__, 'views/templates/hook/ec_cross_selling_order_summary.tpl');        
    }

}
