<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{eccrossselling}prestashop>ajax_footer_ee1d9a1453a91371908a01eb71c0ef78'] = 'Les clients qui ont acheté ce produit ont également acheté';
$_MODULE['<{eccrossselling}prestashop>ajax_footer_dd1f775e443ff3b9a89270713580a51b'] = 'Précédent';
$_MODULE['<{eccrossselling}prestashop>ajax_footer_2d0f6b8300be19cf35e89e66f0677f95'] = 'Ajouter au panier';
$_MODULE['<{eccrossselling}prestashop>ajax_footer_4351cfebe4b61d8aa5efa1d020710005'] = 'Détails';
$_MODULE['<{eccrossselling}prestashop>ajax_footer_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{eccrossselling}prestashop>ajax_order_summary_a863a931b8776ef5a4ccc2bbfc1db855'] = 'Les clients qui ont acheté ces produits ont également acheté';
$_MODULE['<{eccrossselling}prestashop>ajax_order_summary_dd1f775e443ff3b9a89270713580a51b'] = 'Précedent';
$_MODULE['<{eccrossselling}prestashop>ajax_order_summary_2d0f6b8300be19cf35e89e66f0677f95'] = 'Ajouter au panier';
$_MODULE['<{eccrossselling}prestashop>ajax_order_summary_4351cfebe4b61d8aa5efa1d020710005'] = 'Détail';
$_MODULE['<{eccrossselling}prestashop>ajax_order_summary_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{eccrossselling}prestashop>ajax_ee1d9a1453a91371908a01eb71c0ef78'] = 'Les clients qui ont acheté ce produit ont également acheté';
$_MODULE['<{eccrossselling}prestashop>ajax_dd1f775e443ff3b9a89270713580a51b'] = 'Précédent';
$_MODULE['<{eccrossselling}prestashop>ajax_2d0f6b8300be19cf35e89e66f0677f95'] = 'Ajouter au panier';
$_MODULE['<{eccrossselling}prestashop>ajax_4351cfebe4b61d8aa5efa1d020710005'] = 'Détails';
$_MODULE['<{eccrossselling}prestashop>ajax_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_a863a931b8776ef5a4ccc2bbfc1db855'] = 'Les clients qui ont acheté ce produit ont également acheté';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_dd1f775e443ff3b9a89270713580a51b'] = 'Précédent';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_2d0f6b8300be19cf35e89e66f0677f95'] = 'Ajouter au panier';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_10ac3d04253ef7e1ddc73e6091c0cd55'] = 'Suivant';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_4351cfebe4b61d8aa5efa1d020710005'] = 'Détails';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_61b495c73e8b611a583163a961198c65'] = 'Ec Cross Selling';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_f1951a88aca8d53951f15439486dc6ef'] = 'Choisissez les produits que vous voulez pour faire des ventes croisées';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_76034ae5d322d9208399e269394c9a02'] = 'Etes vous sur de vouloir déinstaller le module?';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_f4f70727dc34561dfde1a3c529b6205c'] = 'Options';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_02fda97dba251be414ca3096348247b3'] = 'Afficher les ventes croisées sur la pop-up d\'ajout panier';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activer';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactiver';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_2fd19691b0264608a04c7861232988b2'] = 'Afficher les ventes croisées sur le bas de page produit';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_cd04ef68b7800551beb1361e1dee9716'] = 'Afficher les ventes croisées sur en bas de récapitulatif de commande';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_12d6be0e8e1ea78d0757f5684047008d'] = 'Produits à afficher en bas de récapitulatif de commande';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_5641aa71c4fc82567f62fac8bf3c55f2'] = 'Produits selectionnés du dernier produit ajouté au panier';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_c455de1f2b710abf843d0c835aba0e65'] = 'Produits selectionnés de tous les produits ajouté au panier';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_996624681c3a134999ad5da9ad238605'] = 'Selection de produit spécifique, la même pour tous les paniers';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_d1be48a64ab605463907118ce0e520a5'] = 'Recherche d\'un produit pour la selection de récapitulatif de commande';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_50bbf9345b13fed00429c5d82c7d7d86'] = 'Commencez par taper le nom du produit puis cliquez dessus dans la liste déroulante.';
$_MODULE['<{eccrossselling}prestashop>eccrossselling_c9cc8cce247e49bae79f15173ce97354'] = 'Engregistrer';
$_MODULE['<{eccrossselling}prestashop>configure_5b6cf869265c13af8566f192b4ab3d2a'] = 'Documentation';
$_MODULE['<{eccrossselling}prestashop>configure_ec889acadc14491b189c29e06a8f4185'] = 'Allez dans chaque fiche produit du back office, dans l\'onglet \"Ec Cross Selling\" vous pourrez assigner des produits au produit courant';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_61b495c73e8b611a583163a961198c65'] = 'Ec Cross Selling';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_50bbf9345b13fed00429c5d82c7d7d86'] = 'Commencez par taper le nom du produit puis cliquez dessus dans la liste déroulante.';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_0b69ce3e0d8bbef5b8a2e3763655651b'] = 'N\'oubliez pas d\'enregistrer ensuite!';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_cec5987965b2309363e8da1b9072c9a9'] = 'Recherche d\'un produit';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{eccrossselling}prestashop>tab_cross_selling_9ea67be453eaccf020697b4654fc021a'] = 'Enregistrer et rester';
$_MODULE['<{eccrossselling}prestashop>form_50bbf9345b13fed00429c5d82c7d7d86'] = 'Commencer par taper la première lettre du nom ou de la référence produit, puis celui ci apparaitra dans la liste déroulante.';
$_MODULE['<{eccrossselling}prestashop>form_0b69ce3e0d8bbef5b8a2e3763655651b'] = 'N\'oubliez pas de sauvegarder ensuite le produit';
$_MODULE['<{eccrossselling}prestashop>form_d1be48a64ab605463907118ce0e520a5'] = 'Recherche produit pour la selection de récapitulatif de commande';
$_MODULE['<{eccrossselling}prestashop>blockcart_0c3bf3014aafb90201805e45b5e62881'] = 'Voir mon panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_a85eba4c6c699122b2bb1387ea4813ad'] = 'Panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_deb10517653c255364175796ace3553f'] = 'Produit';
$_MODULE['<{eccrossselling}prestashop>blockcart_068f80c7519d0528fb08e82137a72131'] = 'Produits';
$_MODULE['<{eccrossselling}prestashop>blockcart_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(vide)';
$_MODULE['<{eccrossselling}prestashop>blockcart_4b7d496eedb665d0b5f589f2f874e7cb'] = 'Détails de l\'article';
$_MODULE['<{eccrossselling}prestashop>blockcart_e7a6ca4e744870d455a57b644f696457'] = 'Offert !';
$_MODULE['<{eccrossselling}prestashop>blockcart_ed6e9a09a111035684bb23682561e12d'] = 'supprimer cet article du panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_3d9e3bae9905a12dae384918ed117a26'] = 'Personnalisation n°%d :';
$_MODULE['<{eccrossselling}prestashop>blockcart_09dc02ecbb078868a3a86dded030076d'] = 'Aucun produit';
$_MODULE['<{eccrossselling}prestashop>blockcart_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{eccrossselling}prestashop>blockcart_03e9618cc6e69fe15a57c7377827a804'] = 'A déterminer';
$_MODULE['<{eccrossselling}prestashop>blockcart_c6995d6cc084c192bc2e742f052a5c74'] = 'Livraison gratuite !';
$_MODULE['<{eccrossselling}prestashop>blockcart_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Livraison';
$_MODULE['<{eccrossselling}prestashop>blockcart_ba794350deb07c0c96fe73bd12239059'] = 'Emballage';
$_MODULE['<{eccrossselling}prestashop>blockcart_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'Taxes';
$_MODULE['<{eccrossselling}prestashop>blockcart_96b0141273eabab320119c467cdcaf17'] = 'Total';
$_MODULE['<{eccrossselling}prestashop>blockcart_0d11c2b75cf03522c8d97938490466b2'] = 'Les prix sont TTC';
$_MODULE['<{eccrossselling}prestashop>blockcart_41202aa6b8cf7ae885644717dab1e8b4'] = 'Les prix sont HT';
$_MODULE['<{eccrossselling}prestashop>blockcart_377e99e7404b414341a9621f7fb3f906'] = 'Commander';
$_MODULE['<{eccrossselling}prestashop>blockcart_98b3009e61879600839e1ee486bb3282'] = 'Fermer la fenètre';
$_MODULE['<{eccrossselling}prestashop>blockcart_544c3bd0eac526113a9c66542be1e5bc'] = 'Produit ajouté avec succès au panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantité';
$_MODULE['<{eccrossselling}prestashop>blockcart_e5694b7726ceaf2f057e5f06cf86209e'] = 'Il y a [1]%d[/1] produits dans votre panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_fc86c43dbffcadc193832a310f7a444a'] = 'Il y a un produit dans votre panier';
$_MODULE['<{eccrossselling}prestashop>blockcart_db205f01b4fd580fb5daa9072d96849d'] = 'Produits total';
$_MODULE['<{eccrossselling}prestashop>blockcart_21034ae6d01a83e702839a72ba8a77b0'] = '(HT)';
$_MODULE['<{eccrossselling}prestashop>blockcart_1f87346a16cf80c372065de3c54c86d9'] = '(TTC)';
$_MODULE['<{eccrossselling}prestashop>blockcart_f4e8b53a114e5a17d051ab84d326cae5'] = 'Total livraison';
$_MODULE['<{eccrossselling}prestashop>blockcart_300225ee958b6350abc51805dab83c24'] = 'Poursuivre les achats';
$_MODULE['<{eccrossselling}prestashop>blockcart_7e0bf6d67701868aac3116ade8fea957'] = 'Passer la commande';
$_MODULE['<{eccrossselling}prestashop>blockcart_20351b3328c35ab617549920f5cb4939'] = 'Personalisation #';
