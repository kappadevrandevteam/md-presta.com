<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*/
    include_once('../../config/config.inc.php');
    include_once('../../init.php');
    include(dirname(__FILE__).'/eccrossselling.php');
    /*if (Tools::getValue('token') != Configuration::get('EC_TOKEN_CRISS'))
    {
        Tools::redirect('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        Tools::redirect('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');

        Tools::redirect('Cache-Control: no-store, no-cache, must-revalidate');
        Tools::redirect('Cache-Control: post-check=0, pre-check=0', false);
        Tools::redirect('Pragma: no-cache');

        Tools::redirect('Location: ../');
        exit;
    }*/
    if (Tools::getValue('img'))
    {
        $explode = explode(Tools::getValue('bas'), Tools::getValue('img'));
        $explode = explode('-', $explode[1]);
        $id_shop = Tools::getValue('id_shop');
        $id_lang = Tools::getValue('id_lang');
        $id = Db::getInstance()->getValue('SELECT id_product FROM `'._DB_PREFIX_.'image` WHERE id_image = '.(int)$explode[0]);
        $products = Db::getInstance()->executeS('SELECT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product = '.(int)$id.' AND id_shop = '.(int)$id_shop);
        $html = '';
        $module = new eccrossselling();
        
        if (count($products) > 0)
        {
            $PS_CATALOG_MODE = Configuration::get('PS_CATALOG_MODE');
            $width = 192 * ( ( 2 * count($products) ) - 4 );
            $html .= '<h2 style="margin-left:25px">'.$module->l('Customers who bought this product also bought').' :</h3>
                    <span id="blockcart_scroll_left" class="blockcart_scroll_left" title="'.$module->l('Previous').'" style="display:none;font-size: 25px;left: 5%;position: absolute;top: 33%;">
                        <i class="icon-chevron-circle-left left"></i>
                    </span>
                    <div  id="blockcart_list">
                        <ul class = "slider" '.(count($products) > 4?'style="width: '.$width.'px;position:relative"':'').'>';
            foreach ($products as $id_product)
            {
                $product = new Product((int)$id_product['id_combination'], false, $id_lang, $id_shop, null);
                $link = new Link();
                $url = $link->getProductLink($product);
                $name = $product->name;
                $id_image = Db::getInstance()->getValue('SELECT id_image FROM `'._DB_PREFIX_.'image` WHERE id_product = '.(int)$id_product['id_combination'].' AND cover = 1');
                $type = 'home';
                $type2 = 'default';
                $image = $link->getImageLink($product->link_rewrite, $id_image, $type.'_'.$type2);
                $addcart = $link->getPageLink('cart', true, null, 'add=1&id_product='.(int)$id_product['id_combination'].'&token='.Tools::getValue('static_token'), false);
                $html .= '<li id="ec_slide" '.(count($products) > 4?'style="float: left;"':'style="float: none;"').'>
                            <div class="product-image-container layer_cart_img">
                                <a href="'.$url.'" title="'.$name.'" class="lnk_img">
                                    <img src="http://'.$image.'" alt="'.$name.'" />
                                </a>
                            </div>
                            <h5 itemprop="name"><a class="product-name" href="'.$url.'" title="'.$name.'">'.mb_strimwidth($name, 0, 22, '...').'</a></h5>';
                            if ($product->show_price == 1 && !$PS_CATALOG_MODE)
                                $html .= '<p><div class="content_price">
                                                <span class="price product-price">'.$product->getPrice(true, null, 2).' '.Tools::getValue('sign').'</span>
                                               </div></p>';
                                
                $html .= '    <div class="button-container" >
                                <p>
                                <a title="'.$module->l('Add to cart').'" href="'.$addcart.'" class="button ajax_add_to_cart_button btn btn-default" rel="nofollow" data-id-product="'.(int)$id_product['id_combination'].'">
                                    <span>'.$module->l('Add to cart').'</span>
                                </a>
                                </p>
                                <p>
                                <a title="'.$module->l('View').'" href="'.$url.'" class="button lnk_view btn btn-default">
                                    <span>'.$module->l('View').'</span>
                                </a>
                                </p>
                            </div>
                         </li>';
            }
            
            $html .= '  </ul>
                    </div>
                    <span id="blockcart_scroll_right" class="blockcart_scroll_right" title="'.$module->l('Next').'" style="font-size: 25px;right: 5%;position: absolute;top: 33%;'.(count($products) < 5?'display:none;':'').'">
                        <i class="icon-chevron-circle-right right"></i>
                    </span>';
        }
        echo $html;
                                                        
        
    }