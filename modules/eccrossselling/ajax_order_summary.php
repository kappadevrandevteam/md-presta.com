<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL Ether Création
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL Ether Création is strictly forbidden.
* In order to obtain a license, please contact us: contact@ethercreation.com
* ...........................................................................
* INFORMATION SUR LA LICENCE D'UTILISATION
*
* L'utilisation de ce fichier source est soumise a une licence commerciale
* concedee par la societe Ether Création
* Toute utilisation, reproduction, modification ou distribution du present
* fichier source sans contrat de licence ecrit de la part de la SARL Ether Création est
* expressement interdite.
* Pour obtenir une licence, veuillez contacter la SARL Ether Création a l'adresse: contact@ethercreation.com
* ...........................................................................
*  @package eccrossselling
*  @copyright Copyright (c) 2010-2015 S.A.R.L Ether Création (http://www.ethercreation.com)
*  @author Arthur R.
*  @license Commercial license
*/
    include_once('../../config/config.inc.php');
    include_once('../../init.php');
    include(dirname(__FILE__).'/eccrossselling.php');

    $id_shop = Tools::getValue('id_shop');
    $id_lang = Tools::getValue('id_lang');
    $id_cart = Tools::getValue('id_cart');
    $page_type = Tools::getValue('page_type');
    $order_footer_type = Configuration::get('ECCROSSSELLING_ORDER_FOOTER_TYPE');
    $products = array();
    if ($order_footer_type == 0)
    {
        $id = Db::getInstance()->getValue('SELECT id_product FROM `'._DB_PREFIX_.'cart_product` WHERE id_shop = '.(int)$id_shop.' AND id_cart = '.(int)$id_cart.' ORDER BY date_add DESC');
        $products = Db::getInstance()->executeS('SELECT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product = '.(int)$id.' AND id_shop = '.(int)$id_shop);
    }
    elseif ($order_footer_type == 1)
    {
        $id_tab_sql = Db::getInstance()->executeS('SELECT id_product FROM `'._DB_PREFIX_.'cart_product` WHERE id_shop = '.(int)$id_shop.' AND id_cart = '.(int)$id_cart);
        $id_tab = array();
        foreach ($id_tab_sql as $id)
            $id_tab[] = $id['id_product'];
        if (count($id_tab) > 0)
            $products = Db::getInstance()->executeS('SELECT DISTINCT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product IN ('.pSQL(implode(',', $id_tab)).') AND id_shop = '.(int)$id_shop);
    }
    elseif ($order_footer_type == 2)
        $products = Db::getInstance()->executeS('SELECT id_combination FROM `'._DB_PREFIX_.'eccrossselling` WHERE id_product = 0 AND id_shop = '.(int)$id_shop);
    $html = '';
    $module = new eccrossselling();
    
    if (count($products) > 0)
    {
        $PS_CATALOG_MODE = Configuration::get('PS_CATALOG_MODE');
        $width = 192 * ( ( 2 * count($products) ) - 4 );
        $html .= '
                <h3 class="page-product-heading">'.$module->l('Customers who bought this products also bought').' :</h3>
                <span id="blockcart_scroll_left_footer" class="blockcart_scroll_left_footer" title="'.$module->l('Previous').'" style="display:none;font-size: 25px;left: 5%;position: absolute;top: 33%;">
                    <i class="icon-chevron-circle-left left"></i>
                </span>
                <div id="blockcart_list_footer">
                    <ul class = "slider" '.(count($products) > 4?'style="width: '.$width.'px;position:relative"':'').'>';
        
        foreach ($products as $id_product)
        {
            $product = new Product((int)$id_product['id_combination'], false, $id_lang, $id_shop, null);
            $link = new Link();
            $url = $link->getProductLink($product);
            $name = $product->name;
            $id_image = Db::getInstance()->getValue('SELECT id_image FROM `'._DB_PREFIX_.'image` WHERE id_product = '.(int)$id_product['id_combination'].' AND cover = 1');
            $type = 'home';
            $type2 = 'default';
            $image = $link->getImageLink($product->link_rewrite, $id_image, $type.'_'.$type2);
            $addcart = $link->getPageLink('cart', true, null, 'add=1&id_product='.(int)$id_product['id_combination'].'&token='.Tools::getValue('static_token'), false);
            $html .= '<li id="ec_slide" '.(count($products) > 4?'style="float: left;"':'style="float: none;"').'>
                        <div id="div_footer" class="product-image-container">
                            <a href="'.$url.'" title="'.$name.'" class="lnk_img">
                                <img id="img_footer" src="http://'.$image.'" alt="'.$name.'" />
                            </a>
                        </div>
                        <h5 itemprop="name"><a class="product-name" href="'.$url.'" title="'.$name.'">'.mb_strimwidth($name, 0, 22, '...').'</a></h5>';
                        if ($product->show_price == 1 && !$PS_CATALOG_MODE)
                            $html .= '<p><div class="content_price">
                                            <span class="price product-price">'.$product->getPrice(true, null, 2).' '.Tools::getValue('sign').'</span>
                                           </div></p>';
                            
            $html .= '    <div class="button-container" >
                            <p>
                            <a title="'.$module->l('Add to cart').'" href="'.$addcart.'" class="button ajax_add_to_cart_button btn btn-default" rel="nofollow" data-id-product="'.(int)$id_product['id_combination'].'">
                                <span>'.$module->l('Add to cart').'</span>
                            </a>
                            </p>
                            <p>
                            <a title="'.$module->l('View').'" href="'.$url.'" class="button lnk_view btn btn-default">
                                <span>'.$module->l('View').'</span>
                            </a>
                            </p>
                        </div>
                     </li>';
        }
        
        $html .= '  </ul>
                </div>
                <span id="blockcart_scroll_right_footer" class="blockcart_scroll_right_footer" title="'.$module->l('Next').'" style="font-size: 25px;right: 5%;position: absolute;top: 33%;'.(count($products) < 5?'display:none;':'').'">
                    <i class="icon-chevron-circle-right right"></i>
                </span>
                ';
    }
    echo $html;
