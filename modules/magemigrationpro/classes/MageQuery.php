<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from MigrationPro
 * Use, copy, modification or distribution of this source file without written
 * license agreement from the MigrationPro is strictly forbidden.
 * In order to obtain a license, please contact us: contact@migration-pro.com
 *
 * INFORMATION SUR LA LICENCE D'UTILISATION
 *
 * L'utilisation de ce fichier source est soumise a une licence commerciale
 * concedee par la societe MigrationPro
 * Toute utilisation, reproduction, modification ou distribution du present
 * fichier source sans contrat de licence ecrit de la part de la MigrationPro est
 * expressement interdite.
 * Pour obtenir une licence, veuillez contacter la MigrationPro a l'adresse: contact@migration-pro.com
 *
 * @author    MigrationPro
 * @copyright Copyright (c) 2012-2019 MigrationPro
 * @license   Commercial license
 * @package   MigrationPro: Magento To PrestaShop Migration Tool
 */

class MageQuery
{
    // --- Query builder vars:

    public $version;
    public $entityTypeId;
    protected $source_cart;
    protected $tp;
    protected $offset;
    protected $row_count = 10;
    protected $languages;
    protected $recent_data = false;
    // --- Constructor / destructor:

    public function __construct()
    {
    }

    // --- Configuration methods:

    public function setRowCount($number)
    {
        $this->row_count = (int)$number;
    }

    public function setLanguages($string)
    {
        $this->languages = pSQL($string);
    }

    public function getVersion($number)
    {
        $this->version = (int)$number;
    }

    public function setVersion($number)
    {
        $this->version = (int)$number;
    }

    public function setCart($string)
    {
        $this->source_cart = pSQL($string);
    }

    public function setPrefix($string)
    {
        $this->tp = pSQL($string);
    }

    public function setOffset($number)
    {
        $this->offset = (int)$number;
    }

    public function setRecentData($recent_data)
    {
        $this->recent_data = (bool)$recent_data;
    }

    // --- get all attribute codes

    public function getAllAttributes($typeIds = null)
    {
        if ($typeIds) {
            return 'SELECT attribute_id, attribute_code, entity_type_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id IN (' . pSQL($typeIds). ')';
        }
        return 'SELECT attribute_id, attribute_code, entity_type_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id IN (' . (int)$this->entityTypeId . ')';
    }

    public function saveAttributeIds($attributeIdsArray)
    {
        foreach ($attributeIdsArray as $attribute) {
            if ($attribute['entity_type_id'] == 2) {
                $attribute['attribute_code'] .= '_adr_id';
            } else {
                $attribute['attribute_code'] .= '_id';
            }
            $this->{$attribute['attribute_code']} = (int)$attribute['attribute_id'];
        }

        return true;
    }

    public function getEntityTypeIds()
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_entity_type';
    }

    // --- get query string methods:

    public function getDefaultShopValues()
    {
        $q = array();
        $q['default_lang'] = "SELECT store_id FROM " . pSQL($this->tp) . "store WHERE store_id != " . pSQL('0') . " AND code = '" . pSQL('default') . "'";
        if ($this->version == 1) {
            $q['default_lang'] = "SELECT store_id FROM " . pSQL($this->tp) . "core_store WHERE store_id != " . pSQL('0') . " AND code = '" . pSQL('default') . "'";
        }
        $q['default_currency'] = "SELECT * FROM " . pSQL($this->tp) . "core_config_data WHERE path =
        '" . pSQL('currency/options/default') . "'";
        $q['root_category'] = "SELECT * FROM " . pSQL($this->tp) . "core_config_data WHERE path =
        '" . pSQL('catalog/category/root_id') . "'";

        return $q;
    }

    public function getMappingInfo()
    {
        $q = array();
        if ($this->version == 1) {
            $q['multi_shops'] = 'SELECT `website_id` AS `source_id`, `name` AS `source_name` FROM  `' . pSQL($this->tp) . 'core_website` WHERE website_id != ' . pSQL('0');
            $q['languages'] = 'SELECT `store_id` AS `source_id`, `name` AS source_name FROM ' . pSQL($this->tp) . 'core_store WHERE store_id != ' . pSQL('0');
        } else {
            $q['multi_shops'] = 'SELECT `website_id` AS `source_id`, `name` AS `source_name` FROM  `' . pSQL($this->tp) . 'store_website` WHERE website_id != ' . pSQL('0');
            $q['languages'] = 'SELECT store_id AS source_id, `name` AS source_name FROM ' . pSQL($this->tp) . 'store WHERE store_id != ' . pSQL('0');
        }
        $q['currencies'] = "SELECT `config_id` AS `source_id`, `value` AS `source_name` FROM " . pSQL($this->tp) . "core_config_data WHERE path = '" . pSQL('currency/options/allow') . "'";
        $q['order_states'] = "SELECT status AS source_id, status AS soruce_name FROM " . pSQL($this->tp) . "sales_order_status";
        $q['customer_groups'] = "SELECT customer_group_id AS source_id, customer_group_code AS source_name FROM " . pSQL($this->tp) . "customer_group";

        return $q;
    }

    public function getCountInfo()
    {
        $q = array();

        if (Tools::getValue('entities_taxes') == 1) {
            if (!Tools::getValue('migrate_recent_data')) {
                $q['taxes'] = 'SELECT COUNT(1) AS `c` FROM ' . pSQL($this->tp) . 'tax_class WHERE class_type = \'' . pSQL('PRODUCT') . '\'';
            }
        }

        if (Tools::getValue('entities_manufacturers') == 1) {
            if (!Tools::getValue('migrate_recent_data')) {
                $q['manufacturers'] = 'SELECT COUNT(1) AS `c` FROM ' . pSQL($this->tp) . 'eav_attribute AS a LEFT
                     JOIN ' . pSQL($this->tp) . 'eav_attribute_option AS ao ON a.attribute_id = ao.attribute_id LEFT JOIN
                     ' . pSQL($this->tp) . 'eav_attribute_option_value
                      AS aov ON ao.option_id = aov.option_id
                     WHERE a.attribute_code = \'' . pSQL('manufacturer') . '\' AND store_id = 0';
            }
        }

        if (Tools::getValue('entities_categories') == 1) {
            if (!Tools::getValue('migrate_recent_data')) {
                $q['categories'] = 'SELECT COUNT(1) AS `c` FROM ' . pSQL($this->tp) . 'catalog_category_entity WHERE parent_id != ' . pSQL('0');
            }
        }

        if (Tools::getValue('entities_products') == 1) {
            $last_migrated_product_id = MageMigrationProMigratedData::getLastId('product');
            if (Tools::getValue('migrate_recent_data')) {
                $q['products'] = 'SELECT COUNT(1) AS `c` FROM ' . pSQL($this->tp) . 'catalog_product_entity AS p  WHERE p.entity_id > ' . (int)$last_migrated_product_id . ' AND p.entity_id NOT IN (SELECT product_id from ' . pSQL($this->tp) . 'catalog_product_super_link) AND type_id NOT IN (\'' . pSQL('bundle') . '\', \'' . pSQL('grouped') . '\')';
            } else {
                $q['products'] = 'SELECT COUNT(1) AS `c` FROM ' . pSQL($this->tp) . 'catalog_product_entity AS p  WHERE p.entity_id NOT IN (SELECT product_id from ' . pSQL($this->tp) . 'catalog_product_super_link) AND type_id NOT IN (\'' . pSQL('bundle') . '\', \'' . pSQL('grouped') . '\')';
            }
        }

        if (Tools::getValue('entities_customers') == 1) {
            $last_migrated_customer_id = MageMigrationProMigratedData::getLastId('customer');
            if (Tools::getValue('migrate_recent_data')) {
                $q['customers'] = 'SELECT count(1) as `c` FROM  `' . pSQL($this->tp) . 'customer_entity` WHERE entity_id > ' . (int)$last_migrated_customer_id;
            } else {
                $q['customers'] = 'SELECT COUNT(1) AS `c` FROM  `' . pSQL($this->tp) . 'customer_entity`';
            }
        }

        if ($this->version == 1) {
            if (Tools::getValue('entities_orders') == 1 && Tools::getIsset('entities_customers') == 1) {
                if (Tools::getValue('migrate_recent_data')) {
                    $last_migrated_order_id = MageMigrationProMigratedData::getLastId('order');
                    $q['orders'] = 'SELECT COUNT(1) AS `c` FROM  `' . pSQL($this->tp) . 'sales_flat_order` WHERE entity_id > ' . (int)$last_migrated_order_id;
                } else {
                    $q['orders'] = 'SELECT COUNT(1) AS `c` FROM  `' . pSQL($this->tp) . 'sales_flat_order`';
                }
            }
        } else {
            if (Tools::getValue('entities_orders') == 1 && Tools::getIsset('entities_customers') == 1) {
                if (Tools::getValue('migrate_recent_data')) {
                    $last_migrated_order_id = MageMigrationProMigratedData::getLastId('order');
                    $q['orders'] = 'SELECT COUNT(1) AS `c` FROM  `' . pSQL($this->tp) . 'sales_order` WHERE entity_id > ' . (int)$last_migrated_order_id;
                } else {
                    $q['orders'] = 'SELECT COUNT(1) AS `c` FROM  `' . pSQL($this->tp) . 'sales_order`';
                }
            }
        }

        /*if (Tools::getIsset('entities_reviews') && Tools::getIsset('entities_products')) {
            $q['reviews'] = 'SELECT count(1) AS `c` FROM  `' . pSQL($this->tp) . 'product_comment`';
        }*/

        return $q;
    }

    // --- Tax methods:

    public function taxRulesGroup()
    {
        return 'SELECT class_id AS id_tax_rules_group, class_name AS name FROM ' . pSQL($this->tp) . 'tax_class WHERE class_type = \'' . pSQL('PRODUCT') . '\' ORDER
         BY class_id
        ASC
        LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
    }

    public function taxSqlSecond($taxRulesGroupIds)
    {
        return 'SELECT tax_calculation_id AS id_tax_rule, r.tax_calculation_rate_id AS id_tax, product_tax_class_id as
        id_tax_rules_group, r.tax_country_id AS id_country, s.code AS id_state,
        r.rate, r.zip_from AS zipcode_from, r.zip_to AS zipcode_to, r.tax_postcode, r.zip_is_range from ' . pSQL($this->tp) . 'tax_calculation AS c 
        LEFT JOIN ' . pSQL($this->tp) . 'tax_calculation_rate AS r ON c.tax_calculation_rate_id = r.tax_calculation_rate_id 
        LEFT JOIN ' . pSQL($this->tp) . 'directory_country_region AS s ON region_id = r.tax_region_id
        WHERE product_tax_class_id IN(' . pSQL($taxRulesGroupIds) . ')';
    }

    public function taxSqlThird($taxRateIds)
    {
        $q = array();

        $q['tax_rate'] = 'SELECT tax_calculation_rate_id AS id_tax, code AS name, rate FROM ' . pSQL($this->tp) . 'tax_calculation_rate WHERE tax_calculation_rate_id IN(' . pSQL($taxRateIds) . ')';
        $q['tax_rate_lang'] = 'SELECT * FROM ' . pSQL($this->tp) . 'tax_calculation_rate_title WHERE tax_calculation_rate_id IN (' . pSQL($taxRateIds) . ')';

        return $q;
    }


    // --- Manufactures methods:

    public function getManufacturerAttributeId()
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute WHERE attribute_id = ' . (int)$this->manufacturer_id;
    }

    public function getManufacturerAttributeOptionIds($attribute_id)
    {

        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute_option WHERE attribute_id = ' . (int)$attribute_id . ' ORDER BY option_id
        ASC LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
    }

    public function getManufacturerShopLang($option_ids)
    {
        $q = array();
        $q['manufacturers'] = 'SELECT aov.option_id AS id_manufacturer, aov.value AS name FROM ' . pSQL($this->tp) . 'eav_attribute_option_value AS aov
        WHERE option_id  IN (' . pSQL($option_ids) . ')';

        return $q;
    }

    // --- Category methods:

    public function parentCategoriesStoresWebsites()
    {
        if ($this->version == 1) {
            return 'SELECT * FROM ' . pSQL($this->tp) . 'core_store_group WHERE website_id != ' . pSQL('0');
        }

        return 'SELECT * FROM ' . pSQL($this->tp) . 'store_group WHERE website_id != ' . pSQL('0');
    }

    public function category()
    {
        return 'SELECT entity_id AS id_category,
                       parent_id AS id_parent,
                       created_at AS date_add,
                       updated_at AS date_upd,
                       position,
                       level,
                       path
                       FROM
        ' . pSQL($this->tp) . 'catalog_category_entity WHERE parent_id != ' . pSQL('0') . ' ORDER BY entity_id ASC LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
    }

    public function singleCategory($id_category)
    {
        return 'SELECT entity_id AS id_category,
                       parent_id AS id_parent,
                       created_at AS date_add,
                       updated_at AS date_upd,
                       position,
                       level,
                       path
                       FROM ' . pSQL($this->tp) . 'catalog_category_entity WHERE entity_id = ' . (int)$id_category;
    }

    public function categorySqlSecond($category_ids)
    {
        $q = array();

        $q['category_image'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_category_entity_varchar WHERE entity_id IN (' . pSQL($category_ids) . ') AND attribute_id = ' . (int)$this->image_id;
        $q['category_active_value'] = 'SELECT entity_id AS id_category, value AS active FROM ' . pSQL($this->tp) . 'catalog_category_entity_int WHERE
        entity_id IN (' . pSQL($category_ids) . ') AND attribute_id IN (' . (int)$this->is_active_id . ') AND store_id = 0';

        $q['all_category_attributes'] = 'SELECT attribute_id, attribute_code, entity_type_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id = ' . (int)$this->entityTypeId;

        $q['category_name'] = 'SELECT * FROM
        ' . pSQL($this->tp) . 'catalog_category_entity_varchar
        WHERE attribute_id IN (' . (int)$this->name_id . ') AND
        entity_id IN (' . pSQL($category_ids) . ')';
        $q['category_link_rewrite_meta_title'] = 'SELECT * FROM
        ' . pSQL($this->tp) . 'catalog_category_entity_varchar
        WHERE attribute_id IN (' . (int)$this->url_key_id . ',' . (int)$this->meta_title_id . ') AND
        entity_id IN (' . pSQL($category_ids) . ')';

        $q['category_description_meta_description_meta_keywords'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_category_entity_text WHERE entity_id IN (' . pSQL($category_ids) . ') AND attribute_id IN (' . (int)$this->meta_description_id . ',' . (int)$this->meta_keywords_id . ',' . (int)$this->description_id . ')';

        return $q;
    }


    // --- Product method:

    // selects all products exclude childs
    public function products()
    {
        $last_migrated_product_id = MageMigrationProMigratedData::getLastId('product');
        if ($this->recent_data) {
            return 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity
            WHERE entity_id > ' . (int)$last_migrated_product_id . ' AND entity_id NOT IN (SELECT product_id FROM ' . pSQL($this->tp) . 'catalog_product_super_link) AND type_id NOT IN (\'bundle\', \'grouped\') ORDER BY entity_id ASC LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
        } else {
            return 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity
            WHERE entity_id NOT IN (SELECT product_id FROM ' . pSQL($this->tp) . 'catalog_product_super_link) AND type_id NOT IN (\'bundle\', \'grouped\') ORDER BY entity_id ASC LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
        }
    }

    // selects all attributes with code and ids
    public function allAttributes()
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute';
    }

    //  for images
//    public function allProducts()
//    {
//        return 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity';
//    }


    public function productSqlSecond($id_product)
    {
        $q = array();

        // selects deafult attribute set for product
        $q['default_attribute_set'] = 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute_set WHERE entity_type_id = ' . (int)$this->entityTypeId . ' AND attribute_set_name = \'' . pSQL('Default') . '\'';
        // selects product shop
        $q['product_website'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_website WHERE product_id IN (' . pSQL($id_product) . ')';
        // gets all product attributes
        $q['all_attributes'] = 'SELECT attribute_id, attribute_code, entity_type_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id = ' . (int)$this->entityTypeId;
        // select products weight and prices
        $q['product_weight_price_special_price_cost'] = 'SELECT attribute_id, entity_id, value FROM ' . pSQL($this->tp) . 'catalog_product_entity_decimal WHERE entity_id IN (' . pSQL($id_product) . ')';
        // select all attributes of parent products
        $q['all_product_attributes'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_int WHERE entity_id IN (' . pSQL($id_product) . ')';

        $q['product_tax_rule_groups'] = 'SELECT entity_id, value FROM ' . pSQL($this->tp) . 'catalog_product_entity_int
                                       WHERE entity_id IN (' . pSQL($id_product) . ') AND attribute_id = ' . (int)$this->tax_class_id_id;

//        $q['product_prices'] = 'SELECT DISTINCT entity_id, final_price FROM ' . pSQL($this->tp) . 'catalog_product_index_price';
        $q['product_name_meta_title_meta_description_link_rewrite'] = 'SELECT *
                              FROM  ' . pSQL($this->tp) . 'catalog_product_entity_varchar AS pev
                              WHERE pev.entity_id IN (' . pSQL($id_product) . ') AND pev.attribute_id IN (' . (int)$this->name_id . ',' . (int)$this->meta_title_id . ',' . (int)$this->meta_description_id . ',' . (int)$this->url_key_id . ')';

        $eanId = empty($this->ean_id) ? '0' : $this->ean_id;
        $q['product_ean'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_varchar WHERE entity_id IN (' . pSQL($id_product) . ') AND attribute_id IN (' . (int)$eanId . ')';

//        $q['product_description'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_text WHERE entity_id IN (' . pSQL($id_product) . ')';
        $q['products_quantity'] = 'SELECT * FROM ' . pSQL($this->tp) . 'cataloginventory_stock_item WHERE product_id IN (' . pSQL($id_product) . ')';

        $q['product_visibility'] = 'SELECT entity_id, value FROM ' . pSQL($this->tp) . 'catalog_product_entity_int WHERE attribute_id = ' . (int)$this->visibility_id . ' AND product_id IN (' . pSQL($id_product) . ')';

        $q['specific_price'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_decimal AS pd 
                                WHERE attribute_id = ' . (int)$this->special_price_id . ' AND pd.entity_id IN (' . pSQL($id_product) . ') AND value != ' . pSQL('0');

        $q['specific_price_date'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_datetime AS pt
                                  WHERE entity_id IN (' . pSQL($id_product) . ') AND attribute_id IN (' . (int)$this->special_from_date_id . ',' . (int)$this->special_to_date_id . ')';

        if ($this->version == 2) {
            $q['customer_group_prices'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_tier_price AS tp
                                  WHERE tp.entity_id IN (' . pSQL($id_product) . ')';
        } else {
            $q['customer_group_prices'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_group_price AS tp
                                  WHERE tp.entity_id IN (' . pSQL($id_product) . ')';
            $q['tier_prices'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_tier_price AS tp
                                  WHERE tp.entity_id IN (' . pSQL($id_product) . ')';
        }

        $q['product_status'] = 'SELECT * FROM `' . pSQL($this->tp) . 'catalog_product_entity_int` WHERE `entity_id` IN (' . pSQL($id_product) . ') AND attribute_id = 97';
        $q['category_product'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_category_product WHERE product_id IN (' . pSQL($id_product) . ')';
        $q['stock_available'] = 'SELECT * FROM ' . pSQL($this->tp) . 'cataloginventory_stock_status WHERE product_id IN (' . pSQL($id_product) . ')';

        // selects attributes that exists IN combination
        $q['attribute_group'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_super_attribute WHERE product_id IN (' . pSQL($id_product) . ')';
        $q['attribute_group_lang'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_super_attribute AS psa LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute AS a ON psa.attribute_id = a.attribute_id WHERE product_id IN (' . pSQL($id_product) . ')';
        $q['product_tag'] = 'SELECT * FROM ' . pSQL($this->tp) . 'tag_relation AS tr WHERE tr.product_id IN (' . pSQL($id_product) . ')';


        // selects child products (product_attribute = child_product)
        $q['product_attribute'] = 'SELECT pchilds.product_id AS id_product_attribute, pchilds.parent_id AS id_product, stock.qty AS quantity, stock.backorders AS out_of_stock, weight.value AS weight  
                                    FROM ' . pSQL($this->tp) . 'catalog_product_super_link AS pchilds
                                    LEFT JOIN ' . pSQL($this->tp) . 'cataloginventory_stock_item AS stock ON stock.product_id = pchilds.product_id
                                    LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_entity_decimal AS weight ON weight.entity_id = pchilds.product_id
                                    WHERE pchilds.parent_id IN (' . pSQL($id_product) . ') and weight.attribute_id IN (' . (int)$this->weight_id . ')';

        $q['product_attribute_sku'] = 'SELECT product_id as entity_id, sku FROM ' . pSQL($this->tp) . 'catalog_product_super_link AS pchilds
                                        LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_entity AS p ON pchilds.product_id = p.entity_id
                                        WHERE pchilds.parent_id IN (' . pSQL($id_product) . ')';

        // selects combination attributes
        $q['product_attribute_ids'] = 'SELECT product_id AS id_product, attribute_id
                                               FROM ' . pSQL($this->tp) . 'catalog_product_super_attribute AS sa
                                               WHERE sa.product_id IN (' . pSQL($id_product) . ') ';

        // selects downloadable products
        $q['product_download'] = 'SELECT * FROM ' . pSQL($this->tp) . 'downloadable_link l 
         LEFT JOIN ' . pSQL($this->tp) . 'downloadable_link_title AS lt ON l.link_id = lt.link_id
         WHERE l.product_id IN (' . pSQL($id_product) . ')';

        return $q;
    }

    public function productSqlThird($id_attribute_group, $id_product, $id_tag, $attribute_ids, $id_product_attributes, $defaultAttributeSetId)
    {
        $q = array();

        $eanId = empty($this->ean_id) ? 0 : $this->ean_id;
        $q['combination_ean'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_varchar WHERE attribute_id IN (' . (int)$eanId . ') AND entity_id IN (' . $id_product_attributes . ')';

        $q['product_meta_keyword_description_short_description'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_text AS pet WHERE pet.entity_id IN (' . pSQL($id_product) . ') AND pet.attribute_id IN (' . (int)$this->meta_keyword_id . ',' . (int)$this->description_id . ',' . (int)$this->short_description_id . ')';

        // selects all images accodingly to all products that selected IN productAdditional method
        if ($this->version == 2) {
            $q['image'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery AS emg 
                      LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery_value AS emgv ON emg.value_id = emgv.value_id 
                      WHERE emgv.entity_id IN (' . pSQL($id_product) . ') OR emgv.entity_id IN (SELECT product_id FROM ' . pSQL($this->tp) . 'catalog_product_super_link WHERE parent_id IN (' . pSQL($id_product) . '))';
        } else {
            $q['image'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery AS emg 
                      LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery_value AS emgv ON emg.value_id = emgv.value_id 
                      WHERE emg.entity_id IN (' . pSQL($id_product) . ') OR emg.entity_id IN (SELECT product_id FROM ' . pSQL($this->tp) . 'catalog_product_super_link WHERE parent_id IN (' . pSQL($id_product) . '))';
        }

        // product cover image
        $q['image_cover']='SELECT * from ' . pSQL($this->tp) . 'catalog_product_entity_varchar WHERE attribute_id IN (SELECT attribute_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id=4 AND attribute_code="image") AND entity_id IN (' . pSQL($id_product) . ')';

        // selects child products prices
        $q['product_attribute_prices'] = 'SELECT entity_id, attribute_id, value
                                               FROM ' . pSQL($this->tp) . 'catalog_product_entity_decimal 
                                               WHERE entity_id IN (' . pSQL($id_product_attributes) . ') AND attribute_id = ' . (int)$this->price_id;
        // selects all default groups
        $q['default_attribute_groups'] = 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute_group WHERE attribute_set_id IN (' . pSQL($defaultAttributeSetId) . ')';

        $q['attribute_option_values'] = 'SELECT aov.* FROM ' . pSQL($this->tp) . 'eav_attribute_option AS ao
                           LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option_value AS aov ON aov.option_id = ao.option_id
                           WHERE  ao.attribute_id IN (' . pSQL($id_attribute_group) . ') AND ao.option_id IS NOT NULL AND aov.value IS NOT NULL';
        if ($this->version == 1) {
            $q['attribute'] = 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute_option AS ao
                           LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option_value AS aov ON aov.option_id = ao.option_id
                           WHERE  ao.attribute_id IN (' . pSQL($id_attribute_group) . ') AND ao.option_id IS NOT NULL AND aov.value IS NOT NULL';
        } else {
            $q['attribute'] = 'SELECT ao.*, aos.value AS color FROM ' . pSQL($this->tp) . 'eav_attribute_option AS ao
                           LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option_swatch AS aos ON aos.option_id = ao.option_id
                           WHERE  ao.attribute_id IN (' . pSQL($id_attribute_group) . ') AND ao.option_id IS NOT NULL';
        }

        $q['attribute_lang'] = 'SELECT ao.*, aov.* FROM ' . pSQL($this->tp) . 'eav_attribute_option AS ao
                           LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option_value AS aov ON aov.option_id = ao.option_id
                           WHERE  ao.attribute_id IN (' . pSQL($id_attribute_group) . ') AND ao.option_id IS NOT NULL AND aov.value IS NOT NULL';


        $q['attribute_shop'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_entity_int AS pei WHERE  pei.attribute_id IN (' . pSQL($id_attribute_group) . ') and pei.entity_id IN (' . pSQL($id_product) . ')';
        $q['tag'] = 'SELECT * FROM ' . pSQL($this->tp) . 'tag WHERE tag_id IN (' . pSQL($id_tag) . ') ';


        $q['product_attribute_combination'] = 'SELECT entity_id AS id_product_attribute, value AS id_attribute
                                               FROM ' . pSQL($this->tp) . 'catalog_product_entity_int AS pei
                                               WHERE pei.entity_id IN (' . pSQL($id_product_attributes) . ') AND attribute_id IN (' . pSQL($attribute_ids) . ')';


        // select child products image ids and paths
        if ($this->version == 2) {
            $q['product_attribute_image'] = 'SELECT gallery.entity_id AS id_product_attribute, i.value_id AS id_image
                                               FROM ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery_value_to_entity AS gallery
                                               LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery AS i ON gallery.value_id = i.value_id
                                               WHERE gallery.entity_id IN (' . pSQL($id_product_attributes) . ')';
        } else {
            $q['product_attribute_image'] = 'SELECT i.entity_id AS id_product_attribute, i.value_id AS id_image
                                               FROM ' . pSQL($this->tp) . 'catalog_product_entity_media_gallery AS i 
                                               WHERE i.entity_id IN (' . pSQL($id_product_attributes) . ')';
        }

        $q['product_attribute_shop'] = 'SELECT * FROM  ' . pSQL($this->tp) . 'catalog_product_super_link AS sl
                                        LEFT JOIN ' . pSQL($this->tp) . 'catalog_product_website AS pw ON sl.product_id = pw.product_id
                                        WHERE sl.parent_id IN (' . pSQL($id_product) . ')';

        return $q;
    }

    // selects all default attributes
    public function defaultAttributeIds($attribute_group_ids)
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_entity_attribute WHERE attribute_group_id IN (' . pSQL($attribute_group_ids) . ')';
    }

    // selects all custom attributes
    public function customAttributeIds($default_attribute_ids)
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute WHERE attribute_id NOT IN (' . pSQL($default_attribute_ids) . ',' . (int)$this->manufacturer_id . ') AND entity_type_id = ' . (int)$this->entityTypeId . ' OR attribute_id IN (' . (int)$this->weight_id . (isset($this->width_id) && $this->width_id !== null ? ',' . (int)$this->width_id : '') . (isset($this->height_id) && $this->height_id !== null ? ',' . (int)$this->height_id : '') . ')';
    }

    // selects all custom attributes
    public function allCustomAttributesOptionValues($custom_attribute_values)
    {
        return 'SELECT * FROM ' . pSQL($this->tp) . 'eav_attribute_option_value as aov LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option as ao on aov.option_id = ao.option_id WHERE ao.attribute_id IN (' . pSQL($custom_attribute_values) . ')';
    }

    // --- Order method:

    public function order()
    {
        $tableName = '';
        if ($this->version == 1) {
            $tableName = 'sales_flat_order';
        } else {
            $tableName = 'sales_order';
        }
        $recentData = '';

        if ($this->recent_data) {
            $last_migrated_order_id = MageMigrationProMigratedData::getLastId('order');
            $recentData = ' WHERE entity_id > ' . (int)$last_migrated_order_id;
        }

        return 'SELECT
                      entity_id AS id_order,
                      quote_id AS id_cart,
                      shipping_address_id,
                      billing_address_id,
                      order_currency_code AS id_currency,
                      customer_id AS id_customer,
                      discount_amount,
                      total_paid,
                      subtotal AS total_products,
                      shipping_amount AS total_shipping,
                      created_at AS date_add,
                      updated_at AS date_upd,
                      store_id AS id_shop,
                      state AS current_state,
                      tax_amount,
                      shipping_amount,
                      shipping_tax_amount,
                      total_invoiced AS total_paid_real,
                      shipping_amount,
                      shipping_incl_tax,
                      order_currency_code,
                      subtotal_incl_tax,
                      total_qty_ordered,
                      grand_total, 
                      customer_email,
                      customer_firstname,
                      customer_lastname
                      FROM ' . pSQL($this->tp) . pSQL($tableName) . $recentData . ' ORDER BY id_order ASC LIMIT  ' . (int)$this->offset . ',' . (int)$this->row_count;
    }


    public function orderSqlSecond($order_ids)
    {
        $q = array();
        if ($this->version == 1) {
            $q['order_item'] = 'SELECT distinct * FROM ' . pSQL($this->tp) . 'sales_flat_order_item WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_invoice'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_flat_invoice WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_invoice_grid'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_flat_invoice_grid WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_order_payment'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_flat_order_payment WHERE parent_id IN (' . pSQL($order_ids) . ')';
            $q['address'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_flat_order_address WHERE parent_id IN (' . pSQL($order_ids) . ')';
            $q['order_shop_and_state'] = 'select * from ' . pSQL($this->tp) . 'sales_flat_order_grid WHERE entity_id IN (' . pSQL($order_ids) . ')';
        } else {
            $q['order_item'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_order_item WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_invoice'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_invoice WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_invoice_grid'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_invoice_grid WHERE order_id IN (' . pSQL($order_ids) . ')';
            $q['sales_order_payment'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_order_payment WHERE parent_id IN (' . pSQL($order_ids) . ')';
            $q['address'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_order_address WHERE parent_id IN (' . pSQL($order_ids) . ')';
            $q['order_shop_and_state'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_order_grid WHERE entity_id IN (' . pSQL($order_ids) . ')';
        }

        $q['product_and_product_atributes'] = 'SELECT * FROM ' . pSQL($this->tp) . 'catalog_product_super_link';
        if ($this->version == 1) {
            $q['all_websites'] = 'SELECT * FROM ' . pSQL($this->tp) . 'core_website';
        } else {
            $q['all_websites'] = 'SELECT * FROM ' . pSQL($this->tp) . 'store_website';
        }

        return $q;
    }

    public function orderSqlThird($order_ids, $product_ids)
    {
        $q = array();
        $q['invoice_tax'] = 'SELECT * FROM ' . pSQL($this->tp) . 'sales_order_tax WHERE order_id IN (' . pSQL($order_ids) . ')';
        $q['products_wholesale_prices'] = 'SELECT pd.* FROM ' . pSQL($this->tp) . 'catalog_product_entity_decimal AS pd 
                                                        LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute AS ea ON ea.attribute_id = pd.attribute_id 
                                                        WHERE entity_id IN (' . pSQL($product_ids) . ') AND ea.attribute_code = \'' . pSQL('cost') . '\'';

        return $q;
    }

    // --- Customer methods:

    public function customers()
    {
        $recentData = '';
        $last_migrated_customer_id = MageMigrationProMigratedData::getLastId('customer');
        if ($this->recent_data) {
            $recentData = ' WHERE entity_id > ' . (int)$last_migrated_customer_id;
        }

        if ($this->version == 1) {
            return 'SELECT entity_id AS id_customer,
                       email,
                       is_active AS active,  
                       group_id AS id_default_group,
                       created_at AS date_add,
                       updated_at AS date_upd,
                       website_id AS id_shop
                       FROM ' . pSQL($this->tp) . 'customer_entity ' . $recentData . ' ORDER BY entity_id ASC
        LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
        }

        return 'SELECT entity_id AS id_customer,
                       lastname,
                       firstname,
                       email,
                       website_id AS id_shop,
                       store_id,
                       password_hash AS passwd,
                       gender AS id_gender,
                       is_active AS active,  
                       group_id AS id_default_group,
                       created_at AS date_add,
                       updated_at AS date_upd,
                       store_id,
                       dob AS birthday
                       FROM ' . pSQL($this->tp) . 'customer_entity ' . $recentData . ' ORDER BY entity_id ASC
        LIMIT ' . (int)$this->offset . ',' . (int)$this->row_count;
    }

    public function customerInfos($id_customer)
    {
        $q = array();

        if ($this->version == 1) {
            $q['newsletter'] = 'SELECT * FROM ' . pSQL($this->tp) . 'newsletter_subscriber WHERE customer_id IN (' . pSQL($id_customer) . ')';
        }
        $q['all_attributes'] = 'SELECT attribute_id, attribute_code, entity_type_id FROM ' . pSQL($this->tp) . 'eav_attribute WHERE entity_type_id IN (' . pSQL('1,2') . ')';
        $q['customer_gender'] = 'SELECT * FROM ' . pSQL($this->tp) . 'customer_entity_int AS cei LEFT JOIN ' . pSQL($this->tp) . 'eav_attribute_option_value AS aov ON aov.value_id = cei.value WHERE attribute_id = ' . (int)$this->gender_id . ' AND entity_id IN (' . (int)$id_customer . ')';
        $q['customer_infos'] = 'SELECT * FROM ' . pSQL($this->tp) . 'customer_entity_varchar WHERE attribute_id IN (' . (int)$this->lastname_id . ',' . (int)$this->firstname_id . ',' . (int)$this->password_hash_id . ',' . (int)$this->dob_id . ') AND entity_id IN (' . pSQL($id_customer) . ')';

        return $q;
    }

    public function address($id_customers)
    {
        if ($this->version == 1) {
            return 'SELECT
                        entity_id AS id_address,
                        parent_id AS id_customer,
                        
                        is_active,
                        created_at AS date_add,
                        updated_at AS date_upd
                        FROM ' . pSQL($this->tp) . 'customer_address_entity
        WHERE parent_id IN (' . pSQL($id_customers) . ')';
        }

        return 'SELECT
                        entity_id AS id_address,
                        parent_id AS id_customer,
                        country_id AS id_country, 
                        region AS id_state,
                        company,
                        lastname,
                        firstname,
                        vat_id AS vat_number,
                        city,
                        street, 
                        postcode,
                        telephone AS phone,
                        is_active,
                        created_at AS date_add,
                        updated_at AS date_upd
                        FROM ' . pSQL($this->tp) . 'customer_address_entity
        WHERE parent_id IN (' . pSQL($id_customers) . ')';
    }

    public function customerAddressInfos($id_address)
    {
        $q = array();

        $q['customer_main_infos'] = 'SELECT * FROM ' . pSQL($this->tp) . 'customer_address_entity_varchar WHERE entity_id IN (' . pSQL($id_address) . ')';

        $q['customer_street'] = 'SELECT * FROM ' . pSQL($this->tp) . 'customer_address_entity_text WHERE entity_id IN (' . pSQL($id_address) . ')';

        return $q;
    }
}
