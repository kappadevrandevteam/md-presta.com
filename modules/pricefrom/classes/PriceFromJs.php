<?php
/**
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*/

class PriceFromJs extends ObjectModel
{
    public $id_pricefrom_js;
    public $parent_selector;
    public $hide_element_selector;
    public $active = true;
    public $date_add;
    public $date_upd;
    
    public static $definition = array(
        'table' => 'pricefrom_js',
        'primary' => 'id_pricefrom_js',
        'fields' => array(
            'parent_selector' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 128),
            'hide_element_selector' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 128),
            'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
            'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate')
        )
    );
    
    public static function createTable()
    {
        Db::getInstance()->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'pricefrom_js` (
                `id_pricefrom_js` tinyint(4) NOT NULL AUTO_INCREMENT,
                `parent_selector` varchar(128) NOT NULL DEFAULT "",
                `hide_element_selector` varchar(128) NOT NULL DEFAULT "",
                `active` tinyint(1) UNSIGNED NOT NULL DEFAULT "0",
                `date_add` datetime NOT NULL,
                `date_upd` datetime NOT NULL,
                PRIMARY KEY (`id_pricefrom_js`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
        ');
        
        return true;
    }
    
    public static function dropTable()
    {
        Db::getInstance()->execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'pricefrom_js');
        
        return true;
    }
    
    public static function getAll()
    {
        return Db::getInstance()->executeS('
            SELECT *
            FROM `'._DB_PREFIX_.'pricefrom_js`
            ORDER BY `id_pricefrom_js`
        ');
    }
    
    public static function getActive()
    {
        return Db::getInstance()->executeS('
            SELECT *
            FROM `'._DB_PREFIX_.'pricefrom_js`
            WHERE `active`=1
            ORDER BY `id_pricefrom_js`
        ');
    }
}
