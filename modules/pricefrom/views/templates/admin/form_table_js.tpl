{*
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*}

<table class="table table-bordered">
	<thead>
		<tr>
			<th class="text-center">ID</th>
			<th>{l s='JQuery selector of product block' mod='pricefrom'}</th>
			<th>{l s='JQuery selector of elements to hide' mod='pricefrom'}</th>
			<th class="text-center">{l s='Active' mod='pricefrom'}</th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<td class="text-center">+</td>
			<td><input type="text" name="parent_selector" value="{$parent_selector}" /></td>
			<td><input type="text" name="hide_element_selector" value="{$hide_element_selector}" /></td>
			<td class="text-center">
				<select name="active">
					<option value="0"{if !$active} selected="selected"{/if}>{l s='No' mod='pricefrom'}</option>
					<option value="1"{if $active} selected="selected"{/if}>{l s='Yes' mod='pricefrom'}</option>
				</select>
			</td>
			<td>
				<button type="submit" class="btn btn-default btn-block">{l s='Save' mod='pricefrom'}</button>
			</td>
		</tr>
	</thead>
	<tbody>
		{if $data}
			{foreach from=$data item="row"}
				<tr>
					<td class="text-center">{$row.id_pricefrom_js}</td>
					<td>{$row.parent_selector}</td>
					<td>{$row.hide_element_selector}</td>
					<td class="text-center">
						{if $row.active}
							<a href="{$active_url|sprintf:$row.id_pricefrom_js:0}">{l s='Yes' mod='pricefrom'}</a>
						{else}
							<a href="{$active_url|sprintf:$row.id_pricefrom_js:1}">{l s='No' mod='pricefrom'}</a>
						{/if}
					</td>
					<td>
						<a href="{$delete_url|sprintf:$row.id_pricefrom_js}" class="btn btn-danger btn-block">{l s='Delete' mod='pricefrom'}</a>
					</td>
				</tr>
			{/foreach}
		{else}
			<tr><td colspan="5">{l s='No entry found...' mod='pricefrom'}</td></tr>
		{/if}
	</tbody>
</table>
