{*
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*}

<div class="alert alert-info" id="pricefrom">
	{* NOT WORK
	<a href="{$clear_cache_url}" target="_parent" class="btn btn-default pull-right">
		<i class="process-icon-eraser"></i> {l s='Clear cache' mod='pricefrom'}
	</a>
	*}
	<img src="../modules/pricefrom/logo.png" />
	<h2><strong>{l s='Price from' mod='pricefrom'}</strong></h2>
	<p>
		<strong>{l s='Contact support' mod='pricefrom'}: </strong>
		<a href="{$support_url}" target="blank">{$support_url}</a>
	</p>
	<div class="clearfix"></div>
</div>

<div class="alert alert-warning">
	<p>
		<span>{l s='After any product price changes or module settings, it is recommended to clean the Prestashop cache.' mod='pricefrom'}</span>
	</p>
</div>

<style>
	{literal}
	#pricefrom img {float:left; margin-right:15px; height:60px;}
	#pricefrom h2 {margin-top:0;}
	{/literal}
</style>