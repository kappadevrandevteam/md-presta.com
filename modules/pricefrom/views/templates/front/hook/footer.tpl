{*
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*}
<script type="text/javascript">
{literal}
var priceFromConfig = {
	jsNode: '{/literal}{$js_node nofilter}{literal}',
	jsRules: '{/literal}{$js_rules nofilter}{literal}'
};
{/literal}

if (typeof(priceFrom) == "object") {
	priceFrom.init();
}

</script>
