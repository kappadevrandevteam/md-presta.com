{*
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*}
{if $display_type == 1}
<div class="pricefrom-miniature only">
{else}
<div class="pricefrom-miniature">
{/if}
	{$price_text|sprintf:"<span class=\"pricefrom-price\">$price</span>" nofilter}
</div>
