/**
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*/

priceFrom = {
	initialized: false,
	init: function() {
		if (typeof(priceFromConfig) != "object" || priceFrom.initialized) {
			return;
		}
		
		priceFrom.initialized = true;
		priceFromConfig.jsRules = $.parseJSON(priceFromConfig.jsRules);
		
		this.updateDisplay();
		
		console.log("PriceFrom: DOMNodeInserted on: " + priceFromConfig.jsNode + " - Found: " + $(priceFromConfig.jsNode).length);
		
		$(priceFromConfig.jsNode).bind('DOMNodeInserted', function(event) {
			priceFrom.updateDisplay();
		});
	},
	
	updateDisplay: function() {
		if (!$('.pricefrom-miniature.only').length) {
			return;
		}
		
		console.log("PriceFrom: Udpate display...");
		
		$.each(priceFromConfig.jsRules, function(key, rule) {
			var found = $('.pricefrom-miniature.only')
			.parents(rule.parentSelector)
			.find(rule.hideElementSelector).length;
			
			console.log("PriceFrom: Rule (parentSelector: " + rule.parentSelector + " / hideElementSelector: " + rule.hideElementSelector + ") - Found: " + found);
			
			$('.pricefrom-miniature.only')
			.parents(rule.parentSelector)
			.find(rule.hideElementSelector)
			.css('cssText', 'display: none !important');
			
			$('.pricefrom-miniature.only').parents(rule.parentSelector).addClass('pricefrom-miniature-only');
		});
	}
};

priceFrom.init();
