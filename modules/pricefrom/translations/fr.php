<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pricefrom}prestashop>pricefrom_d4b764e109c6721dab6ba130e056fa52'] = 'Prix à partir de';
$_MODULE['<{pricefrom}prestashop>pricefrom_94812aec1b70190b5993f4f11218dc1d'] = 'Affiche le prix le plus bas du produit si les déclinaisons ont des prix différents.';
$_MODULE['<{pricefrom}prestashop>pricefrom_2466b4b39501f174db88cb8442c56d93'] = 'Le champ \"Texte du prix\" (%s) est requis.';
$_MODULE['<{pricefrom}prestashop>pricefrom_18931ab56817286425593af8d724628e'] = 'Le champ \"Texte du prix\" (%s)  doit contenir \"%s\" (qui correspond au prix).';
$_MODULE['<{pricefrom}prestashop>pricefrom_6b29a87bfbb74c95a9722781ef683a53'] = 'Le champ \"%s\" est requis.';
$_MODULE['<{pricefrom}prestashop>pricefrom_99097d43e6e24a914140a30d001becea'] = 'JQuery selector du bloc produit';
$_MODULE['<{pricefrom}prestashop>pricefrom_f3fd8e2f0ce3aabcd53fb8cc7041eb7d'] = 'La valeur du champ \"%s\" est trop long (128 catactères).';
$_MODULE['<{pricefrom}prestashop>pricefrom_0784033b02337bb1a4f3bf6497c2daa3'] = 'JQuery selector des élements à cacher';
$_MODULE['<{pricefrom}prestashop>pricefrom_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Actif';
$_MODULE['<{pricefrom}prestashop>pricefrom_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{pricefrom}prestashop>pricefrom_c1406eca45706d582258e4dc3828f168'] = 'Impossible de supprimer cette règle...';
$_MODULE['<{pricefrom}prestashop>pricefrom_708522cc42e10b08227f195e1d0f19ce'] = 'Impossible d\'éditer cette règle...';
$_MODULE['<{pricefrom}prestashop>pricefrom_94a3d467754f2b5fd74f2d43bbb2ef40'] = 'Impossible d\'écrire le fichier: %s';
$_MODULE['<{pricefrom}prestashop>pricefrom_d6462d607a7ff4c9bed41c53abd8fc52'] = 'Le cache a bien été supprimé';
$_MODULE['<{pricefrom}prestashop>pricefrom_3225a10b07f1580f10dee4abc3779e6c'] = 'Paramètres';
$_MODULE['<{pricefrom}prestashop>pricefrom_a383aa8b158413d3af8b16094bedb86d'] = 'Texte du prix';
$_MODULE['<{pricefrom}prestashop>pricefrom_8d738b40ab30686e8b67e8e2a958dc44'] = 'Important, n\'oublier pas d\'insérer dans la ligne \"%s\" (exemple: A partir de %s = A partir de xx.xx)';
$_MODULE['<{pricefrom}prestashop>pricefrom_0b646e5726db30d9a32bb717be97d5ca'] = 'Include les remises par quantité';
$_MODULE['<{pricefrom}prestashop>pricefrom_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{pricefrom}prestashop>pricefrom_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{pricefrom}prestashop>pricefrom_8031af8765f90e1c6c67e124617130bd'] = 'Afficher même si le prix trouvé est le même que le prix affiché';
$_MODULE['<{pricefrom}prestashop>pricefrom_365119d429fc230c8e9b5c3c94842c19'] = 'Type d\'affichage';
$_MODULE['<{pricefrom}prestashop>pricefrom_eb92025cb8c66f1850c13a9b602a1856'] = 'Tout afficher';
$_MODULE['<{pricefrom}prestashop>pricefrom_2671440a994444f110f4538ce5def656'] = 'Afficher uniquement prix à partir de';
$_MODULE['<{pricefrom}prestashop>pricefrom_5d109d277c237079796de4a676dc48a3'] = 'Afficher dans la page de détail du produit';
$_MODULE['<{pricefrom}prestashop>pricefrom_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{pricefrom}prestashop>pricefrom_c8f7b254da8d5b22fc4b0a8581278b1a'] = 'Style CSS personnalisé';
$_MODULE['<{pricefrom}prestashop>pricefrom_2c56c360580420d293172f42d85dfbed'] = 'CSS';
$_MODULE['<{pricefrom}prestashop>pricefrom_4dac77d23002b1bfd562ed129443dbc4'] = 'Configuration Javascript (utilisateur avancé uniquement)';
$_MODULE['<{pricefrom}prestashop>pricefrom_41d8c619ed076a213d57d83eebda3c80'] = 'Bloc pouvant contenir des blocs de produit (JQuery selector)';
$_MODULE['<{pricefrom}prestashop>pricefrom_c93810cc577be549a326d4a802f02c83'] = 'Jquery selector';
$_MODULE['<{pricefrom}prestashop>pricefrom_7a1920d61156abc05a60135aefe8bc67'] = 'Défaut';
$_MODULE['<{pricefrom}prestashop>pricefrom_1f4da964f8eab62e96e8cfe406e44364'] = 'Règles';
$_MODULE['<{pricefrom}prestashop>form_header_79c0d6cba080dc90b01c887064c9fc2f'] = 'Vider le cache';
$_MODULE['<{pricefrom}prestashop>form_header_d4b764e109c6721dab6ba130e056fa52'] = 'Prix à partir de';
$_MODULE['<{pricefrom}prestashop>form_header_ad119d5ea30f6c26b58e0af5bc43c9fe'] = 'Contacter le support';
$_MODULE['<{pricefrom}prestashop>form_header_6fadbfb3e66c0f5509bdb2340961c410'] = 'Après toutes modifications de prix produits ou de paramètres du module, il est recommandé de vider le cache de Prestashop.';
$_MODULE['<{pricefrom}prestashop>form_table_js_99097d43e6e24a914140a30d001becea'] = 'JQuery selector du bloc produit';
$_MODULE['<{pricefrom}prestashop>form_table_js_0784033b02337bb1a4f3bf6497c2daa3'] = 'JQuery selector des élements à cacher';
$_MODULE['<{pricefrom}prestashop>form_table_js_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Actif';
$_MODULE['<{pricefrom}prestashop>form_table_js_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{pricefrom}prestashop>form_table_js_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{pricefrom}prestashop>form_table_js_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{pricefrom}prestashop>form_table_js_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{pricefrom}prestashop>form_table_js_f5b923a8b1e1e0c7837ba25137406313'] = 'Aucun entrée trouvée...';
