<?php
/**
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_0_2($object)
{
    Configuration::updateValue('PRICEFROM_DISPLAY_PRODUCT_PAGE', 1);
    
    return true;
}
