<?php
/**
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_0_0($object)
{
    if (!PriceFromJs::createTable() || !$object->registerHook('footer')) {
        return false;
    }
    
    Configuration::updateValue('PRICEFROM_JS_NODE', (version_compare(_PS_VERSION_, '1.7', '<')) ? '#center_column' : '#products');
    
    $js = new PriceFromJs();
    $success = false;
    
    if (version_compare(_PS_VERSION_, '1.7', '<')) {
        $js->parent_selector = '.ajax_block_product';
        if (Configuration::get('PRICEFROM_JS_PARENT') !== false && Configuration::get('PRICEFROM_JS_TO_HIDE') !== false) {
            $js->hide_element_selector = Configuration::get('PRICEFROM_JS_PARENT')." ".Configuration::get('PRICEFROM_JS_TO_HIDE');
        } else {
            $js->hide_element_selector = '.content_price > *:not(.pricefrom-miniature)';
        }
        
        $success = $js->add();
    } else {
        $js->parent_selector = '.js-product-miniature';
        if (Configuration::get('PRICEFROM_JS_PARENT') !== false && Configuration::get('PRICEFROM_JS_TO_HIDE') !== false) {
            $js->hide_element_selector = Configuration::get('PRICEFROM_JS_PARENT')." ".Configuration::get('PRICEFROM_JS_TO_HIDE');
        } else {
            $js->hide_element_selector = '.product-price-and-shipping > *:not(.pricefrom-miniature)';
        }
        $success = $js->add();
    }
    
    Configuration::deleteByName('PRICEFROM_JS_PARENT');
    Configuration::deleteByName('PRICEFROM_JS_TO_HIDE');
    
    return $success;
}
