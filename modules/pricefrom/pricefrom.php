<?php
/**
* MODULE PRESTASHOP - PRICEFROM
*
* LICENSE :
* All rights reserved
* COPY AND REDISTRIBUTION FORBIDDEN WITHOUT PRIOR CONSENT FROM LSDev
* LICENCE :
* Tous droits réservés, le droit d'auteur s'applique
* COPIE ET REDISTRIBUTION INTERDITES SANS ACCORD EXPRES DE LSDev
*
* @author    LSDev
* @copyright 2020 LSDev
* @license   Proprietary - no redistribution without authorization
*/

if (!defined('_PS_VERSION_')) {
    exit();
}

require_once(_PS_MODULE_DIR_.'/pricefrom/classes/PriceFromJs.php');

class Pricefrom extends Module
{
    private $support_url = 'https://addons.prestashop.com/contact-form.php?id_product=30107';
    private $default_js_node;
    private $base_css_file = false;
    private $post_errors = array();
    private $success_js_added = false;
    
    public function __construct()
    {
        $this->name = 'pricefrom';
        $this->author = 'LSDev';
        $this->tab = 'front_office_features';
        $this->version = '2.0.2';
        $this->module_key = 'ead945123504e0287392685c477809a5';
        $this->bootstrap = true;
        parent::__construct();
        
        $this->displayName = $this->l('Price from');
        $this->description = $this->l('Displays the lowest price of the product if the declinations have different prices.');
        $this->ps_versions_compliancy = array('min' => '1.6.0.14', 'max' => _PS_VERSION_);
        
        $this->default_js_node = (version_compare(_PS_VERSION_, '1.7', '<')) ? '#center_column' : '#products';
        
        $this->base_css_file = _PS_MODULE_DIR_.$this->name.'/views/css/';
    }
    
    public function install()
    {
        if (!parent::install()
        || !$this->registerHook('displayProductPriceBlock')
        || !$this->registerHook('header')
        || !$this->registerHook('footer')
        || !PriceFromJs::createTable()
        ) {
            return false;
        }
        
        $languages = Language::getLanguages(false);
        $price_text = array();
        
        foreach ($languages as $lang) {
            $price_text[$lang['id_lang']] = 'Price from %s';
        }
        
        Configuration::updateValue('PRICEFROM_PRICE_TEXT', $price_text);
        Configuration::updateValue('PRICEFROM_DISPLAY_TYPE', 0);
        Configuration::updateValue('PRICEFROM_DISPLAY_ALWAYS', 0);
        Configuration::updateValue('PRICEFROM_DISPLAY_PRODUCT_PAGE', 1);
        Configuration::updateValue('PRICEFROM_INCLUDE_QTY_DISCOUNT', 0);
        Configuration::updateValue('PRICEFROM_CUSTOM_CSS', "");
        Configuration::updateValue('PRICEFROM_JS_NODE', $this->default_js_node);
        
        $js = new PriceFromJs();
        
        if (version_compare(_PS_VERSION_, '1.7', '<')) {
            $js->parent_selector = '.ajax_block_product';
            if (Configuration::get('PRICEFROM_JS_PARENT') !== false && Configuration::get('PRICEFROM_JS_TO_HIDE') !== false) {
                $js->hide_element_selector = Configuration::get('PRICEFROM_JS_PARENT') + " " + Configuration::get('PRICEFROM_JS_TO_HIDE');
                Configuration::deleteByName('PRICEFROM_JS_PARENT');
                Configuration::deleteByName('PRICEFROM_JS_TO_HIDE');
            } else {
                $js->hide_element_selector = '.content_price > *:not(.pricefrom-miniature)';
            }
            
            $js->add();
        } else {
            $js->parent_selector = '.js-product-miniature';
            if (Configuration::get('PRICEFROM_JS_PARENT') !== false && Configuration::get('PRICEFROM_JS_TO_HIDE') !== false) {
                $js->hide_element_selector = Configuration::get('PRICEFROM_JS_PARENT') + " " + Configuration::get('PRICEFROM_JS_TO_HIDE');
                Configuration::deleteByName('PRICEFROM_JS_PARENT');
                Configuration::deleteByName('PRICEFROM_JS_TO_HIDE');
            } else {
                $js->hide_element_selector = '.product-price-and-shipping > *:not(.pricefrom-miniature)';
            }
            $js->add();
        }
        
        return true;
    }
    
    public function uninstall()
    {
        if (!parent::uninstall()
        || !PriceFromJs::dropTable()
        || !Configuration::deleteByName('PRICEFROM_PRICE_TEXT')
        || !Configuration::deleteByName('PRICEFROM_DISPLAY_TYPE')
        || !Configuration::deleteByName('PRICEFROM_DISPLAY_ALWAYS')
        || !Configuration::deleteByName('PRICEFROM_DISPLAY_PRODUCT_PAGE')
        || !Configuration::deleteByName('PRICEFROM_INCLUDE_QTY_DISCOUNT')
        || !Configuration::deleteByName('PRICEFROM_CUSTOM_CSS')
        || !Configuration::deleteByName('PRICEFROM_JS_NODE')
        ) {
            return false;
        }
        
        return true;
    }
    
    public function getConfigFieldsValues()
    {
        $languages = Language::getLanguages(false);
        $price_text = array();
        $custom_css = Configuration::get('PRICEFROM_CUSTOM_CSS');
        
        foreach ($languages as $lang) {
            $price_text[$lang['id_lang']] = Tools::getValue('price_text_'.$lang['id_lang'], Configuration::get('PRICEFROM_PRICE_TEXT', $lang['id_lang']));
        }
        
        if ($custom_css != "") {
            if (file_exists($this->base_css_file.$custom_css)) {
                $custom_css = Tools::file_get_contents($this->base_css_file.$custom_css);
            } else {
                $custom_css = "";
                Configuration::updateValue('PRICEFROM_CUSTOM_CSS', $custom_css);
            }
        }
        
        return array(
            'price_text' => $price_text,
            'display_type' => Tools::getValue('display_type', Configuration::get('PRICEFROM_DISPLAY_TYPE')),
            'display_always' => Tools::getValue('display_always', Configuration::get('PRICEFROM_DISPLAY_ALWAYS')),
            'display_prodcut_page' => Tools::getValue('display_prodcut_page', Configuration::get('PRICEFROM_DISPLAY_PRODUCT_PAGE')),
            'custom_css' => Tools::getValue('custom_css', $custom_css),
            'include_qty_discount' => Tools::getValue('include_qty_discount', Configuration::get('PRICEFROM_INCLUDE_QTY_DISCOUNT')),
            'js_node' => Tools::getValue('js_node', Configuration::get('PRICEFROM_JS_NODE'))
        );
    }

    private function postValidation()
    {
        if (Tools::isSubmit('submitPricefrom')) {
            $languages = Language::getLanguages(false);
            
            foreach ($languages as $lang) {
                $text = Tools::getValue('price_text_'.$lang['id_lang']);
                
                if ($text == "") {
                    $this->post_errors[] = sprintf($this->l('The "Price text" (%s) field is required.'), $lang['iso_code']);
                } elseif (strpos($text, "%s") === false) {
                    $this->post_errors[] = sprintf($this->l('The "Price text" (%s) require "%s" (corresponds to the price).'), $lang['iso_code'], '%s');
                }
            }
            
            $parent_selector = Tools::getValue('parent_selector');
            $hide_element_selector = Tools::getValue('hide_element_selector');
            $active = (int) Tools::getValue('active');
            
            if ($parent_selector != "" || $hide_element_selector != "") {
                if ($parent_selector == "") {
                    $this->post_errors[] = sprintf($this->l('The field "%s" is required.'), $this->l('JQuery selector of product block'));
                } elseif (Tools::strlen($parent_selector) > 128) {
                    $this->post_errors[] = sprintf($this->l('The field "%s" is to long (128 characters).'), $this->l('JQuery selector of product block'));
                } elseif ($hide_element_selector == "") {
                    $this->post_errors[] = sprintf($this->l('The field "%s" is required.'), $this->l('JQuery selector of elements to hide'));
                } elseif (Tools::strlen($hide_element_selector) > 128) {
                    $this->post_errors[] = sprintf($this->l('The field "%s" is to long (128 characters).'), $this->l('JQuery selector of elements to hide'));
                } elseif (!in_array($active, array(0,1))) {
                    $this->post_errors[] = sprintf($this->l('The field "%s" is required.'), $this->l('Active'));
                }
            }
        }
    }
    
    public function postProcess()
    {
        $output = '';
        
        $this->postValidation();
        
        if (Tools::getValue('deleteJs') !== false) {
            $js = new PriceFromJs((int) Tools::getValue('deleteJs'));
            
            if ($js->delete()) {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            } else {
                $output .= $this->displayError(array(
                    $this->l('Cannot delete this rule...')
                ));
            }
        } elseif (Tools::getValue('activeJs') !== false && Tools::getValue('active') !== false) {
            $js = new PriceFromJs((int)Tools::getValue('activeJs'));
            
            if (Validate::isLoadedObject($js)) {
                $js->active = (int)Tools::getValue('active');
                $js->update();
                
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            } else {
                $output .= $this->displayError(array(
                    $this->l('Cannot edit this rule...')
                ));
            }
        } elseif (count($this->post_errors)) {
            foreach ($this->post_errors as $error) {
                $output .= $this->displayError($error);
            }
        } elseif (Tools::isSubmit('submitPricefrom')) {
            $languages = Language::getLanguages(false);
            $price_text = array();
            $custom_css = Tools::getValue('custom_css');
            
            foreach ($languages as $lang) {
                $price_text[$lang['id_lang']] = Tools::getValue('price_text_'.$lang['id_lang']);
            }
            
            if ($custom_css != "") {
                $new_custom_css = 'pricefrom-custom-'.time().'.css';
                $old_custom_css = Configuration::get('PRICEFROM_CUSTOM_CSS');
                
                if ($old_custom_css != "" && file_exists($this->base_css_file.$old_custom_css)) {
                    unlink($this->base_css_file.$old_custom_css);
                    
                    Configuration::updateValue('PRICEFROM_CUSTOM_CSS', "");
                }
                
                $handle = fopen($this->base_css_file.$new_custom_css, "w+");
                
                if ($handle) {
                    fwrite($handle, $custom_css);
                    fclose($handle);
                    
                    Configuration::updateValue('PRICEFROM_CUSTOM_CSS', $new_custom_css);
                } else {
                    $output .= $this->displayError(sprintf($this->l('Could not write the file: %s'), $this->base_css_file.$new_custom_css));
                }
            } else {
                Configuration::updateValue('PRICEFROM_CUSTOM_CSS', "");
            }
            
            Configuration::updateValue('PRICEFROM_PRICE_TEXT', $price_text);
            Configuration::updateValue('PRICEFROM_DISPLAY_TYPE', (int)Tools::getValue('display_type'));
            Configuration::updateValue('PRICEFROM_DISPLAY_ALWAYS', (int)Tools::getValue('display_always'));
            Configuration::updateValue('PRICEFROM_DISPLAY_PRODUCT_PAGE', (int)Tools::getValue('display_prodcut_page'));
            Configuration::updateValue('PRICEFROM_INCLUDE_QTY_DISCOUNT', (int)Tools::getValue('include_qty_discount'));
            Configuration::updateValue('PRICEFROM_JS_NODE', Tools::getValue('js_node'));
            
            $parent_selector = Tools::getValue('parent_selector');
            $hide_element_selector = Tools::getValue('hide_element_selector');
            $active = (int) Tools::getValue('active');
            
            if ($parent_selector != "" && $hide_element_selector != "" && in_array($active, array(0,1))) {
                $js = new PriceFromJs();
                $js->parent_selector = $parent_selector;
                $js->hide_element_selector = $hide_element_selector;
                $js->active = $active;
                $js->add();
                
                $this->success_js_added = true;
            }
            
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        } elseif (Tools::getValue('clear_cache')) {
            $this->clearCache();
            
            $output .= $this->displayConfirmation($this->l('The cache has been successfully deleted'));
        }

        
        return $output;
    }
    
    public function getContent()
    {
        $output = $this->formHeader();
        $output .= $this->postProcess();
        $output .= $this->renderForm();
        
        return $output;
    }
    
    private function formHeader()
    {
        $this->context->smarty->assign(array(
            'support_url' => $this->support_url,
            'clear_cache_url' =>  $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&clear_cache=1'
        ));
        
        //Return view
        return $this->display(__FILE__, 'views/templates/admin/form_header.tpl');
    }
    
    private function formTable()
    {
        $this->context->smarty->assign(array(
            'parent_selector' => (!$this->success_js_added) ? Tools::getValue('parent_selector', '') : '',
            'hide_element_selector' => (!$this->success_js_added) ? Tools::getValue('hide_element_selector', '') : '',
            'active' => (!$this->success_js_added) ? Tools::getValue('hide_element_selector', 1) : 1,
            'data' => PriceFromJs::getAll(),
            'delete_url' => $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&deleteJs=%d',
            'active_url' => $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&activeJs=%d&active=%d'
        ));
        
        //Return view
        return $this->display(__FILE__, 'views/templates/admin/form_table_js.tpl');
    }
    
    public function renderForm()
    {
        $base_url = $this->context->link->protocol_content.Tools::getMediaServer($this->name)._MODULE_DIR_.$this->name;
        $ps_version = (version_compare(_PS_VERSION_, '1.7', '>=')) ? 'presta17' : 'presta16';
        
        $fields_config_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Parameters'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Price text'),
                        'name' => 'price_text',
                        'lang'   => true,
                        'required' => true,
                        'desc' => $this->l('Important, dont miss "%s" into the line (example: Price from %s = Price form xx.xx)')
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Include quantity discounts prices'),
                        'name'   => 'include_qty_discount',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id'   => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'   => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        'required' => true
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Show even if the price found is the same at the posted price'),
                        'name'   => 'display_always',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id'   => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'   => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        'required' => true
                    ),
                    array(
                        'type'   => 'radio',
                        'label'  => $this->l('Display type'),
                        'name'   => 'display_type',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id'   => 'show_all',
                                'value' => 0,
                                'label' => '<img src="'.$base_url.'/views/img/'.$ps_version.'/with_price.jpg" alt="'.$this->l('Show all').'" />'
                            ),
                            array(
                                'id'   => 'show_only_pricefrom',
                                'value' => 1,
                                'label' => '<img src="'.$base_url.'/views/img/'.$ps_version.'/without_price.jpg" alt="'.$this->l('Show only price from').'" />'
                            )
                        ),
                        'required' => true
                    ),
                    array(
                        'type'   => 'switch',
                        'label'  => $this->l('Display in product detail page'),
                        'name'   => 'display_prodcut_page',
                        'is_bool' => true,
                        'values' => array(
                            array(
                                'id'   => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'   => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                        'required' => true
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right')
            )
        );
        
        $fields_css_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Custom style CSS'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('CSS'),
                        'name' => 'custom_css',
                        'rows' => 6
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right')
            )
        );
        
        $fields_js_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Javascript configuration (for advanced user)'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Block that can contain product blocks (JQuery selector)'),
                        'name' => 'js_node',
                        'required' => true,
                        'desc' => $this->l('Jquery selector').' - '.$this->l('Default').': '.$this->default_js_node
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('Rules'),
                        'name' => $this->formTable()
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right')
            )
        );
        
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitPricefrom';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
        return $helper->generateForm(array($fields_config_form, $fields_css_form, $fields_js_form));
    }
    
    public function hookHeader($params)
    {
        $custom_css = Configuration::get('PRICEFROM_CUSTOM_CSS');
        
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            if ((int)Configuration::get('PRICEFROM_DISPLAY_TYPE') == 1) {
                $this->context->controller->registerJavascript(
                    'modules-pricefrom',
                    $this->_path.'views/js/pricefrom.js',
                    array('position' => 'bottom', 'priority' => 150)
                );
            }
            $this->context->controller->registerStylesheet(
                'modules-pricefrom',
                $this->_path.'views/css/pricefrom17.css',
                array('media' => 'all', 'priority' => 150)
            );
            
            if ($custom_css != "") {
                $this->context->controller->registerStylesheet(
                    'modules-pricefrom-custom',
                    $this->_path.'views/css/'.$custom_css,
                    array('media' => 'all', 'priority' => 150)
                );
            }
        } else {
            if ((int)Configuration::get('PRICEFROM_DISPLAY_TYPE') == 1) {
                $this->context->controller->addJs($this->_path.'views/js/pricefrom.js');
            }
            
            $this->context->controller->addCSS($this->_path.'views/css/pricefrom16.css');
            
            if ($custom_css != "") {
                $this->context->controller->addCSS($this->_path.'views/css/'.$custom_css);
            }
        }
    }
    
    public function hookFooter($params)
    {
        if ((int)Configuration::get('PRICEFROM_DISPLAY_TYPE') == 0) {
            return;
        }
        
        $data = PriceFromJs::getActive();
        $config = array();
        
        foreach ($data as $r) {
            $config[] = (object) array(
                'parentSelector' => $r['parent_selector'],
                'hideElementSelector' => $r['hide_element_selector']
            );
        }
        
        //Set smarty var
        $this->smarty->assign(array(
            'js_node' => Configuration::get('PRICEFROM_JS_NODE'),
            'js_rules' => Tools::jsonEncode($config)
        ));
        
        //Return view
        return $this->display(__FILE__, 'hook/footer.tpl', $this->getCacheId());
    }
    
    public function hookDisplayProductPriceBlock($params)
    {
        //Prestaqhop >= 1.7
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            return $this->renderWidget("displayProductPriceBlock", $params);
        }
        
        //Get controller
        $controller = (isset($this->context->controller->php_self)) ? $this->context->controller->php_self : false;
        
        //Check
        if (!$controller || $controller == "order" || !$params || !isset($params['product'])) {
            return false;
        } else if ((int)Configuration::get('PRICEFROM_DISPLAY_PRODUCT_PAGE') == 0 && $controller == "product") {
            return false;
        }
        
        //Init
        $id_product = (is_object($params['product'])) ? (int)$params['product']->id : (int)$params['product']['id_product'];
        $type = $params['type'];
        $view = ($controller != "product") ? 'hook/product_miniature.tpl' : 'hook/product.tpl';
        $product = new Product((int)$id_product);
        $hook_type = (version_compare(_PS_VERSION_, '1.6.1.0', '<')) ? "price" : "after_price";
        $display_always = (int)Configuration::get('PRICEFROM_DISPLAY_ALWAYS');
        
        //Check object
        if (!Validate::isLoadedObject($product)) {
            return false;
        }
        
        //Check display price
        if (!$this->displayPrices($id_product)) {
            return false;
        }
        
        //Get attribute ID
        if (is_object($params['product']) && isset($params['product']->specificPrice['id_product_attribute'])) {
            $id_product_attribute = (int)$params['product']->specificPrice['id_product_attribute'];
        } elseif (is_array($params['product']) && isset($params['product']['id_product_attribute'])) {
            $id_product_attribute = (int)$params['product']['id_product_attribute'];
        } else {
            $id_product_attribute = (int)$product->getDefaultIdProductAttribute();
        }
        
        //Check hook
        if (($controller == "product" && $type != $hook_type) || ($controller != "product" && $type != "price")) {
            return false;
        }
        
        //Get current price
        $current_price = $this->getCurrentPrice($id_product, $id_product_attribute);
        
        //Check current price
        if ($current_price === false) {
            return false;
        }
        
        //Get lower price
        $lower_price = $this->getLowerPrice($id_product);
        
        //Check lower price
        if ($lower_price === false) {
            return false;
        }
        
        //Check difference
        if (!$display_always && $controller != "product" && $current_price == $lower_price) {
            return false;
        }
        
        //Cache id
        $cache_id = (($controller != "product") ? 'priceform-product-min' : 'priceform-product');
        $cache_id .= '|'.$id_product.'|'.$id_product_attribute;
        $cache_id .= '|'.Configuration::get('PRICEFROM_DISPLAY_TYPE').Configuration::get('PRICEFROM_DISPLAY_ALWAYS').Configuration::get('PRICEFROM_INCLUDE_QTY_DISCOUNT');
        
        //Set smarty var
        $this->smarty->assign(array(
            'price_text' => Configuration::get('PRICEFROM_PRICE_TEXT', $this->context->language->id),
            
            'price' => Tools::displayPrice(Tools::ps_round((float)$lower_price, (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION'))),
            'display_type' => (int)Configuration::get('PRICEFROM_DISPLAY_TYPE')
        ));
        
        //Return view
        return $this->display(__FILE__, $view, $this->getCacheId($cache_id));
    }
    
    /*
    * WIDGET
    */
    public function renderWidget($hookName, array $configuration)
    {
        $controller = (isset($this->context->controller->php_self)) ? $this->context->controller->php_self : false;
        
        if ((int)Configuration::get('PRICEFROM_DISPLAY_PRODUCT_PAGE') == 0 && $controller == "product") {
            return false;
        }
        
        if (($controller == "product" && $configuration['type'] != "after_price")
        || ($controller != "product" && $configuration['type'] != "before_price")) {
            return false;
        }
        
        $lower_price = $this->getWidgetVariables($hookName, $configuration);
        
        if ($lower_price === false) {
            return false;
        }
        
        $view = ($controller != "product") ? 'hook/product_miniature.tpl' : 'hook/product.tpl';
        
        //Set cache id
        $cache_id = (($controller != "product") ? 'priceform-product-min' : 'priceform-product');
        $cache_id .= '|'.$configuration['product']['id_product'];
        $cache_id .= '|'.$configuration['product']['id_product_attribute'];
        $cache_id .= '|'.Configuration::get('PRICEFROM_DISPLAY_TYPE').Configuration::get('PRICEFROM_DISPLAY_ALWAYS').Configuration::get('PRICEFROM_INCLUDE_QTY_DISCOUNT');
        
        //Set smarty var
        $this->smarty->assign(array(
            'price_text' => Configuration::get('PRICEFROM_PRICE_TEXT', $this->context->language->id),
            
            'price' => Tools::displayPrice(Tools::ps_round((float)$lower_price, (int)Configuration::get('PS_PRICE_DISPLAY_PRECISION'))),
            'display_type' => (int)Configuration::get('PRICEFROM_DISPLAY_TYPE')
        ));
        
        //Return view
        return $this->fetch('module:pricefrom/views/templates/front/'.$view, $this->getCacheId($cache_id));
    }
    
    public function getWidgetVariables($hookName, array $configuration)
    {
        //Init
        $controller = (isset($this->context->controller->php_self)) ? $this->context->controller->php_self : false;
        $id_product = (int)$configuration['product']['id_product'];
        $product = new Product((int)$id_product);
        $display_always = (int)Configuration::get('PRICEFROM_DISPLAY_ALWAYS');
        
        //Check object
        if (!Validate::isLoadedObject($product)) {
            return false;
        }
        
        //Check display price
        if (!$this->displayPrices($id_product)) {
            return false;
        }
        
        //Get attribute ID
        if (isset($configuration['product']['attributes'])
        && is_array($configuration['product']['attributes'])
        && count($configuration['product']['attributes'])) {
            $id_product_attribute = (int)$configuration['product']['id_product_attribute'];
        } else {
            $id_product_attribute = (int)$product->getDefaultIdProductAttribute();
        }
        
        //Get current price
        $current_price = $this->getCurrentPrice($id_product, $id_product_attribute);
        
        //Check current price
        if ($current_price === false) {
            return false;
        }
        
        //Get lower price
        $lower_price = $this->getLowerPrice($id_product);
        
        //Check lower price
        if ($lower_price === false) {
            return false;
        }
        
        //Check difference
        if (!$display_always && $controller != "product" && $current_price == $lower_price) {
            return false;
        }
        
        return $lower_price;
    }
    
    private function getCurrentPrice($id_product, $id_product_attribute)
    {
        $cache_id = $this->getCacheId('pricefrom::getCurrentPrice|'.(int)$id_product.'|'.(int)$id_product_attribute);
        
        if (!Cache::isStored($cache_id)) {
            $product = new Product((int)$id_product);
            
            if (!Validate::isLoadedObject($product) || !$product->show_price) {
                return false;
            }
            
            $use_tax = $this->useTax();
            $_price = Product::getPriceStatic((int)$product->id, $use_tax, (int)$id_product_attribute);
            
            Cache::store($cache_id, (float)$_price);
        }
        
        return (float)Cache::retrieve($cache_id);
    }
    
    private function getLowerPrice($id_product)
    {
        $include_qty_discount = (int)Configuration::get('PRICEFROM_INCLUDE_QTY_DISCOUNT');
        $id_customer = (isset($this->context->customer) ? (int)$this->context->customer->id : 0);
        $id_group = (int)Group::getCurrent()->id;
        $id_country = ($id_customer) ? (int)Customer::getCurrentCountry($id_customer) : (int)Tools::getCountry();
        $id_currency = (int) $this->context->cookie->id_currency;
        $id_shop = $this->context->shop->id;
        $prices = array();
        
        $cache_id = $this->getCacheId('pricefrom::getLowerPrice|'.$include_qty_discount.'|'.(int)$id_product);
        
        if (!Cache::isStored($cache_id)) {
            $product = new Product((int)$id_product);
            
            if (!Validate::isLoadedObject($product)) {
                return false;
            }
            
            $use_tax = $this->useTax();
            
            //Product with attribute
            if ((version_compare(_PS_VERSION_, '1.7', '>=') && $product->hasCombinations()) || (version_compare(_PS_VERSION_, '1.7', '<') && $product->hasAttributes())) {
                foreach ($product->getAttributeCombinations($this->context->language->id, false) as $combination) {
                    $_price = Product::getPriceStatic((int)$product->id, $use_tax, (int)$combination['id_product_attribute']);
                    
                    if ($_price !== null && !in_array($_price, $prices)) {
                        $prices[] = $_price;
                    }
                }
            //Product without attribute
            } else {
                $prices[] = Product::getPriceStatic((int)$product->id, $use_tax);
            }
            
            //Get quantity discounts prices
            if ((int)Configuration::get('PRICEFROM_INCLUDE_QTY_DISCOUNT')) {
                $quantity_discounts = SpecificPrice::getQuantityDiscounts($id_product, $id_shop, $id_currency, $id_country, $id_group, false, true, (int)$id_customer);
                
                if (!empty($quantity_discounts)) {
                    foreach ($quantity_discounts as $quantity_discount) {
                        $_price = Product::getPriceStatic((int)$product->id, $use_tax, (int)$quantity_discount['id_product_attribute'], 6, null, false, true, (int)$quantity_discount['from_quantity']);
                        
                        if ($_price !== null && !in_array($_price, $prices)) {
                            $prices[] = $_price;
                        }
                    }
                }
            }
           
            if (count($prices) <= 1) {
                Cache::store($cache_id, false);
            } else {
                //Find best price
                asort($prices);
                reset($prices);
                
                Cache::store($cache_id, (float)current($prices));
            }
        }
        
        return Cache::retrieve($cache_id);
    }
    
    private function displayPrices($id_product = false)
    {
        $group = Group::getCurrent();
        
        if (!$group->show_prices) {
            return false;
        }
        
        if ($id_product !== false) {
            $show_price = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                SELECT `show_price`
                FROM `'._DB_PREFIX_.'product`
                WHERE `id_product` = '.(int)$id_product);
            
            if (!$show_price) {
                return false;
            }
        }
        
        return true;
    }
    
    private function useTax()
    {
        $group = Group::getCurrent();
        
        return (!$group->price_display_method);
    }
    
    private function clearCache()
    {
        //WORKING PROGRESS
        Cache::clean('pricefrom::getCurrentPrice*');
        Cache::clean('pricefrom::getLowerPrice*');
        Tools::clearCache(null, 'product.tpl', 'priceform*');
        Tools::clearCache(null, 'product_miniature.tpl', 'priceform*');
        
        return true;
    }
}
