{*
 * 2008 - 2020 (c) Prestablog
 *
 * MODULE PrestaBlog
 *
 * @author    Prestablog
 * @copyright Copyright (c) permanent, Prestablog
 * @license   Commercial
 * @version    4.3.5
 *}

<!-- Module Presta Blog -->
<div class="prestablog-nav-top">
   <a href="{PrestaBlogUrl}">
      {l s='Blog' mod='prestablog'}
   </a>
</div>
<!-- /Module Presta Blog -->
